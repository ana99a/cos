/*
 * Copyright (c) 2021 Jean Cardinal, Arturo Merino and Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <string>
#include <utility>
#include <vector>

#ifdef NDEBUG
#define DEBUG 0
#else
#define DEBUG 1
#endif

class Elimination_tree {
friend class Tests;

  enum class Direction {up, down, none};

public:
  // last argument of the constructor is set to true if given edge list represents a tree, and to false otherwise
  Elimination_tree(int, std::vector<std::string>&, std::vector<std::pair<int,int>>&, bool&);
  bool next();
  void print();  // print compact representation of elimination tree
  void print_peo();  // print perfect elimination ordering

private:
  int n_;  // number of vertices of the underlying tree and the elimination tree
  std::vector<std::string> vertex_strings_;  // string representations of vertices

  // the underlying tree
  std::vector<std::vector<int>> adjL_;  // adjacency lists
  std::vector<int> peo_;  // perfect elimination order
  std::vector<int> oep_;  // inverse permutation
  std::vector<std::vector<int>> neighbor_to_;  // neighbor_to_[i][j] gives the neighbor of i towards j in the underlying tree
  std::vector<std::vector<int>> child_to_;  // child_to_[i][d]==j if in the elimination tree vertex i has a child j, and in the underlying tree the vertex j is reached from i towards the neighbor d

  // the elimination tree
  int root_;
  std::vector<int> first_child_;  // the first and last child of each vertex (in no particular order)
  std::vector<int> last_child_;
  std::vector<int> smaller_child_;  // if a vertex has a child that is smaller than itself, then we track it
  std::vector<int> parent_;  // the parent of a vertex in the elimination tree
  std::vector<int> left_sibling_;  // left and right siblings of a vertex
  std::vector<int> right_sibling_;

  // bookkeeping for Algorithm J
  std::vector<Direction> o_;  // direction array
  std::vector<int> s_;  // rotation vertex selection

  void rotate(int, Direction);  // tree rotation in the elimination tree
  bool is_root(int i) { return (parent_[i] == 0); }

  // build graph from edge list; returns number of edges
  int build_tree(std::vector<std::pair<int,int>>&);
  // run DFS from given starting vertex, maintain vertex count and label vertices with
  // those counts, record predecessor relations towards the starting vertex
  void dfs(int u, int &count, std::vector<int> &label, std::vector<int> &pred);
  bool is_connected();
  void elimination_order();
  void init_elimination_tree();
  // insert vertex i as the last child of j
  void insert_vertex(int i, int j);
  // delete i from the elimination tree
  void delete_vertex(int i);
  // find minimum child (this is not a constant-time operation, but is only used for debugging)
  int min_child(int i);
  // initialization functions
  void init_neighbor_to();
  void init_child_to();
  void init_bookkeeping();

  void print_rec(int, bool&);
  void check_consistency();
  void check_consistency_rec(int,int&);
};
