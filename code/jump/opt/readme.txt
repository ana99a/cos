This folder contains various optimized versions of the jump algorithm, implemented by Chigozie Agomo:
jump.cpp                 Correctly working code without any optimizations (this is the code running on the website).
jump1.cpp,...,jump7.cpp  Correctly working code with various additional optimizations.
results.pdf              Benchmarking results with various test instances.

The optimizations apply mainly to classical patterns, and to conjunctions of those, and they are automatically enabled/disabled in such a way that the correctness is maintained.

Optimization 1: After a jump of value j, we can ignore values larger than j in searching for a match of the tame pattern. These larger values are always at the boundaries of the permutation.

Optimization 2: The jumping value must be the biggest value in the pattern match.

Optimization 3: When we encountered a pattern match, we can sometimes jump by more than 1 position, as far until we cross the nearest element in the pattern match (so that the match gets destroyed). Pattern matches are searched as far away from the largest value as possible, so that these jumps can potentially be very long.

Optimization 4: Patterns of size 1 and 2 are handled separately.

Optimization 5: When searching for a match of a pattern, we first match the largest value that jumped, and we then first search the partial remaining pattern in one of the two remaining halves (left or right of the largest value), and then we search the other partial remaining pattern in the other half. If the first search does not find a match, we know there is none and we stop. If the first search finds at match, we do the second search. If the second search does not find a match, we know there is none, and if the second search finds a match, we still do a proper search for the entire pattern. The rule which of the two halves is checked first is to take the side for which the corresponding binomial coefficient \binom{size of the half}{size of the partial pattern} is smaller.

Optimization 6: Removed a time waste in the code for optimization 5, yields another 5% improvement.

Optimization 7: This is an implementation of Kozma's `even-odd method' described in his paper `Faster and simpler algorithms for finding large patterns in permutations'.