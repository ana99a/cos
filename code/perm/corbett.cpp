/*
 * Copyright (c) 2018 Joe Sawada, Aaron Williams
 *
 * This file implements an order of permutations in an article by P.F. Corbett
 * "Rotator Graphs: An Efficient Topology for Point-to-Point & Multiprocessor 
 * Networks", IEEE Transactions on Parallel and Distributed Systems, 3 (1992) 
 * 622–626
 *
 * The implementation is by Aaron Williams following a design set by 
 * Torsten Mutze and Jorg Arndt.  More efficient implementations are possible.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <vector>
#include "corbett.hpp"

Corbett::Corbett(int n) : n_(n) {
  pi_.resize(n_+1);  // entry 0 is not used
  perms_total_ = 1;
  for (int i=2; i<=n_; i++) {  // compute n_!
    perms_total_ = perms_total_ * i;
  }  
  first();
}

void Corbett::first() {
  perms_gen_ = 1;
  pi_[0] = 0;
  for (int i=1; i<=n_; i++) {
    pi_[i] = n_-i+1;
  }
}

void Corbett::print() {
  for (int i=1; i<=n_; i++) {
    std::cout << pi_[i] << " ";
  }
}

void Corbett::prefix_rotate(unsigned int length) {
  int tmp = pi_[1];
  for (int i=1; i < length; i++) {
    pi_[i] = pi_[i+1];
  }
  pi_[length] = tmp;
}

bool Corbett::next() {
  if (perms_gen_ == perms_total_) {
    return false;
  }

  int x; // down sequence 
  int y; // up sequence
  int half; // ceil(n_/2)
  
  half = (n_+1)/2;
  
  x = 0;
  while (x+2 <= half && pi_[x+2] == n_-x) x++;

  y = n_;
  while (y >= half+2 && pi_[y] == n_-y+1) y--;
  y = n_-y;

  if (x > y) {
    prefix_rotate(y+2);
  } else {
    prefix_rotate(n_-x);    
  }

  perms_gen_++;
  return true;
}
