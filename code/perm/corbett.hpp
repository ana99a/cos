/*
 * Copyright (c) 2018 
 *
 * This file implements an order of permutations in an article by P.F. Corbett
 * "Rotator Graphs: An Efficient Topology for Point-to-Point & Multiprocessor 
 * Networks", IEEE Transactions on Parallel and Distributed Systems, 3 (1992) 
 * 622–626
 *
 * The implementation is by Aaron Williams following a design set by 
 * Torsten Mutze and Jorg Arndt.  More efficient implementations are possible.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORBETT_HPP
#define CORBETT_HPP

#include <vector>

class Corbett {
public:
  explicit Corbett(int n);
  void first();  // initial permutation is pi_[1...n] = n(n-1)(n-2)(n-3)...1
  void print();  // to make the output consistent with the other algorithms, we shift indices to 0...n-1 in the print function
  bool next();

private:
  void prefix_rotate(unsigned int length);  // rotate the prefix of specified length one position ot the left
  
  int n_;
  std::vector<int> pi_;
  long long perms_gen_;
  long long perms_total_;  // n!
};

#endif
