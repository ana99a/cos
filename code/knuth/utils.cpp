/*
 * Copyright (c) 2020 Arturo Merino, Ondrej Micka, Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "utils.hpp"

int gcd(int a, int b) {
  assert(((a>0 && b>0) || (a==0 && b>0) || (b==0 && a>0)) && "arguments of gcd() should be non-negative and not both 0");
  int ca, cb;
  return ext_gcd(a, b, ca, cb);
}

int ext_gcd(int a, int b, int &ca, int &cb) {
  assert(((a>0 && b>0) || (a==0 && b>0) || (b==0 && a>0)) && "arguments of egcd() should be non-negative and not both 0");
  bool swap = false;
  if (b == 0) {  // swap a and b if b==0
    swap = true;
    int t = a;
    a = b;
    b = t;
  }
  int div, mod;
  ca = 1, cb = 0;
  int da = 0, db = 1;
  int ta, tb;
  do {
    div = a / b;
    mod = a % b;
    a = b;
    b = mod;
    ta = ca - div*da;
    tb = cb - div*db;
    ca = da; cb = db; da = ta; db = tb;
  } while (b != 0);
  if (swap) {  // swap results back
    int t = ca;
    ca = cb;
    cb = t;
  }
  return a;
}

int inverse(int a, int b) {
  assert(b>=2 && "multiplicative inverse does not exist");
  int ca, cb;
  int gcd = ext_gcd(a,b,ca,cb);
  assert((gcd == 1) && "multiplicative inverse does not exist");
  if (gcd != 1) {
    return 0;  // multiplicative inverse does not exist
  }
  return mod(ca, b);
}

int catalan(int n) {
  assert(n>=0 && "Catalan numbers defined only for non-negative indices");
  // use Segner's recurrence relation and take modulo after each step
  std::vector<int> c(n+1);
  c[0] = 1;
  for (int m=1; m<=n; m++) {
    c[m] = 0;
    for (int i=0; i<m; ++i) {
      c[m] = (c[m] + c[i]*c[m-1-i]) % (2*n+1);
    }
  }
  return c[n];
}

void factor(int n, std::vector<int> &factors, std::vector<int> &mult) {
  assert(n>=1 && "can only factor positive numbers");
  factors.clear();
  mult.clear();
  int distinct_factors = 0;
  for (int f=2; f<=n; f++) {
    if ((n % f) == 0) {
      n = n / f;
      factors.push_back(f);
      mult.push_back(1);
      distinct_factors++;
      while ((n % f) == 0) {
        n = n / f;
        mult[distinct_factors-1]++;
      }
    }
  }
}

bool is_prime_power(int n) {
  std::vector<int> factors;
  std::vector<int> mult;
  factor(n, factors, mult);
  return (factors.size() == 1);
}

void print_bitstring(const std::vector<bool> &x) {
  for (int i=0; i<x.size(); ++i) {
    std::cout << x[i];
  }
}