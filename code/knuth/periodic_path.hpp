/*
 * Copyright (c) 2020 Arturo Merino, Ondrej Micka, Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PERIODIC_PATH_HH
#define PERIODIC_PATH_HH

#include <cassert>
#include <vector>
#include <string>
#include "tree.hpp"

class Periodic_path {
  friend class Tests;

private:
  const int n_;             // combinations of length 2n+2 with n+1 many 1s and 0s each
  const int m_;             // actual length of the bitstring m=2n+1 (the first alternating bit is omitted)
  const int s_;             // shift value
  // For n<=38 the shift value is ignored and has to be achieved from the outside of this class by switching,
  // whereas for n>=39 the shift value is respected by following an appropriate spanning tree T_n.

  std::vector<bool> x_;     // current bitstring (the first alternating bit is omitted)
  int dyck_shift_;          // how many left rotations we must perform on x_ to obtain a Dyck word with trailing 0
  std::vector<int> match_;  // indices of matching pairs of {0,1}-bits in the Dyck word, i.e., match_[i] = j iff
                            // x_[i] and x_[j] are a matching pair in (x_ << dyck_shift_); -1 entry at the trailing 0

  Tree *centroid_tree_;  // current rooted tree obtained from the rooted tree (x_ << dyck_shift_) by rerooting it
                         // at centroid with subtrees ordered by (T1) rule from the paper
  Tree *current_tree_;   // current rooted tree (x_ << dyck_shift_)
  // Both trees have the same underlying plane tree, only the choice of root is different.
  // The vertex id's give a bijection between both trees.

  std::vector<Tree::Subtree *> subtrees_;  // additional info about subtrees of the centroid tree
                                           // i-th entry of this vector contains info about subtree whose root has id=i
                                           // only subtrees of the centroid tree's root are stored, remaining entries are nullptr

  Tree::Subtree *selected_subtree_;  // subtree selected by rule (T2) from the paper

  // The bitflips along the Gray code are computed in pairs of flipping a 0-->1 (from Hamming weight n to n+1)
  // plus a 1-->0 (from Hamming weight n+1 to n). The variable has_more_ones indicates that we are in the second
  // of these two steps.
  // The following invariants are maintained: x_ left-rotated by dyck_shift_ starts with a Dyck path
  // and its last bit is a trailing 0 (in the first step) or a trailing 1 (in the second step).
  // Moreover, the trees always correspond to this Dyck path. Consequently, most updates are done in
  // the first step (update x_, match_, dyck_shift_, rotate tree), whereas in the second step we
  // basically only flip the trailing 0 to a trailing 1.
  bool has_more_ones_;  // true if x_ have more ones than zeros (second step)

  // reversed paths are traversed in backward direction, i.e., by applying f^{-1} rather than f
  bool on_reversed_path_;
  int reversed_steps_remaining_;  // count down steps on reversed path from 4 to 0, when 0 is reached we jump to forward path again

public:
  explicit Periodic_path(int n, int s);  // shift value is ignored for n<=38
  ~Periodic_path();
  // initialize path with given starting bitstring (of the correct length and Hamming weight)
  void init(const std::vector<bool> &start);
  // compute next bitstring in Gray code; returns position of bit flipped in the last step
  int next();

  // Cyclically rotate x_ by r positions to the right and update data structures accordingly.
  // The shift r can be negative (for achieving an effective left rotation), it is taken modulo m_ = x_.size().
  void rotate_right(int r);

  inline bool on_reversed_path() { return on_reversed_path_; }

  inline const std::vector<bool> &get_x() { return x_; }

  // Check if bitstring differs from x_ by cyclic rotation. If yes, the argument
  // right>=0 contains the number of right rotations to apply to bitstring to obtain x_.
  bool is_rotation_of(const std::vector<bool> &bitstring, int &right);
  // compare whether x_ in both paths differ only up to rotation
  bool operator==(const Periodic_path &p) const;
  inline bool operator!=(const Periodic_path &p) const {
    return !(*this == p);
  }

  // only needed for testing
  void init(const std::string &start);  // converts start to vector<bool> and calls the other init

  #ifndef NDEBUG
  void print_x();
  // draw trees into given files, unless filename is an empty string ("")
  void draw_trees(const std::string &centroid_fn, const std::string &current_fn);
  // check consistency of the match_ vector; inconsistency may also mean that x_
  // is not a Dyck word with trailing bit
  bool check_match_consistency();
  #endif

private:
  // auxiliary functions for initialization

  // Return p such that bitstring left-rotated by p positions is a Dyck word with extra trailing 0 (if #0s > #1s),
  // or with extra trailing 1 (if #1s > #0s). Assumes #0s in bitstring equals #0s in x_, otherwise the result does
  // not make sense.
  // This function is also used when comparing x_ with another bitstring, this is why it has an argument.
  int dyck_shift(const std::vector<bool> &bitstring);

  // build match_ vector from x_
  void build_match_vector();

  // Determine location of x_ on a reversed path, returning the distance from the end of a reversed path
  // (this is the value of reversed_steps_remaining), or -1 if x_ is not on a reversed path.
  // Requires all other data structures to be initialized properly, as it checks whether a pull is applicable.
  int locate_reversed_path();

  // Create Subtree objects for subtrees of centroid tree's root and populate subtrees_.
  // Previous content of subtrees_ is removed and Subtree objects pointed to are are deleted.
  void create_subtrees();

  // compute position flipped in x_ when applying f
  int apply_f();
  // compute position flipped in x_ when applying f^{-1}
  int apply_f_inverse();

  // skip reversed path either in forward direction (apply f) or backward direction (apply f^{-1})
  void skip_reversed_path(bool forward, std::vector<int> &flips);

  // tree operations
  enum class Operation {PULL, PUSH, NONE};

  // apply pull/push operations to bitstring and trees
  // pair of bit positions that are flipped is stored in flips vector
  void pull(std::vector<int> &flips);
  void push(std::vector<int> &flips);
  // Apply tree operation only on trees. This is used as a subroutine in pull/push
  // and when checking applicable operations (computation of the spanning tree T_n).
  void modify_tree(Operation op);

  // Can we perform a pull/push operation on the current tree according to the definition of the spanning
  // tree T_n from the paper? NONE is never applicable, but it should also not be used.
  bool is_applicable(Operation op);

  // Auxiliary functions for is_applicable(). In the spanning tree T_n defined in the paper, every plane
  // tree T defines one other plane tree T', obtained by a pull or push operation, with potential
  // phi(T')=phi(T)-1. The star has minimum potential, and is therefore exempt from this rule.
  // All these edges in T_n are directed arcs, oriented from a tree to the one that is obtained
  // from it by the pull operation (so the potential can increase or decrease along the arcs).
  // To check whether the current tree is the endpoint of an arc in T_n, we have to consider two cases:
  // (i) The operation on the current tree leads to a tree with potential -1 compared to myself, and this arc is in the spanning tree;
  bool is_down_arc(Operation op);
  // (ii) The operation on the current tree leads to a tree with potential +1 compared to myself, and this arc is in the spanning tree.
  bool is_up_arc(Operation op);

  // Implementation of rule (T2) from the paper, along with respective part of special dumbbell rule (D).
  // If centroid tree is a star, we return nullptr.
  Tree::Subtree *select_subtree();
  // rules are quite different for the case of one or two centroids, so we split them
  Tree::Subtree *select_subtree_one_centroid();
  Tree::Subtree *select_subtree_two_centroids();

  // Select leaf to pull/push from the subtree according to rule (T3) from the paper, including special
  // treatment of dumbbells (rule (D)).
  // Also returns whether the leaf is to be pulled or pushed. The pointer subtree must
  // give a subtree from the centroid tree.
  // If subtree is nullptr (in case the tree is a star), returns nullptr, NONE.
  std::pair<Tree::Vertex *, Operation> select_leaf(Tree::Subtree *subtree);

  // determine whether current tree x_ is pullable, pushable or neither of the two
  inline Operation get_tree_operation() {
    if (!has_more_ones_ && x_[dyck_shift_] && x_[(dyck_shift_+1) % m_] && !x_[(dyck_shift_+2) % m_]) {  // x_ = 110...
      return Operation::PULL;
    } else if (!has_more_ones_ && x_[dyck_shift_] && !x_[(dyck_shift_+1) % m_] && x_[(dyck_shift_+2) % m_]) {  // x_ = 101...
      return Operation::PUSH;
    } else {
      return Operation::NONE;
    }
  }

  // Find subtree of the centroid tree's root containing the given vertex.
  // If the vertex is the root the centroid tree, return nullptr.
  Tree::Subtree *find_subtree(Tree::Vertex *v);

  // check if there is a symmetry (automorphism) among the centroid subtrees such that current_subtree == selected_subtree_
  bool deal_with_symmetry(Tree::Subtree *current_subtree);
  bool deal_with_symmetry_one_centroid(Tree::Subtree *current_subtree);
  bool deal_with_symmetry_two_centroids(Tree::Subtree *current_subtree);
  // auxiliary function for deal_with_symmetry_two_centroids_() that reroots current_tree_
  void symmetry_reroot(Tree::Vertex *new_root);

private:
  // auxiliary shortcut functions

  // updates match_ so that i and j form matching 10-pair
  inline void match_bits(int i, int j) {
    match_[i] = j;
    match_[j] = i;
  }

  // count 1s and 0s in given bitstring
  static std::pair<int, int> count_bits(std::vector<bool> bitstring);

  // get a vertex of the centroid tree corresponding to the given vertex of the current tree
  inline Tree::Vertex *to_centroid_tree(Tree::Vertex *v) { return centroid_tree_->get_vtx_by_id(v->get_id()); }
  // get a vertex of the current tree corresponding to the given vertex of the centroid tree
  inline Tree::Vertex *to_current_tree(Tree::Vertex *v) { return current_tree_->get_vtx_by_id(v->get_id()); }

  // Return subtree of the centroid's tree root rooted at vertex v.
  // If v is not a child of the root, nullptr is returned.
  inline Tree::Subtree *get_subtree(Tree::Vertex *v) {
    assert(v && "nullptr defines no subtree");
    return subtrees_[v->get_id()];
  }

  // PULL <--> PUSH, NONE <--> NONE
  Operation inverse_operation(Operation op);

  // we are responsible for deallocating the trees and subtrees
  inline void delete_trees() {
    if (centroid_tree_) delete centroid_tree_;
    if (current_tree_) delete current_tree_;
  }

  inline void delete_subtrees() {
    for (Tree::Subtree *s : subtrees_) {
      if (s) delete s;
    }
  }
};

// we need to compare two bitstrings, one stored as vector the other as a list
// complexity is O(min(vec.size(), list.size()))
bool operator==(const std::vector<bool> &vec, const std::list<bool> &list);
inline bool operator==(const std::list<bool> &list, const std::vector<bool> &vec) {
  return vec == list;
}
inline bool operator!=(const std::vector<bool> &vec, const std::list<bool> &list) {
  return !(vec == list);
}
inline bool operator!=(const std::list<bool> &list, const std::vector<bool> &vec) {
  return !(vec == list);
}

#endif