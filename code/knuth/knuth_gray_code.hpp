/*
 * Copyright (c) 2020 Arturo Merino, Ondrej Micka, Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KNUTH_GRAY_CODE_HH
#define KNUTH_GRAY_CODE_HH

#include <vector>
#include <tuple>
#include <functional>
#include "periodic_path.hpp"
#include "small_code.hpp"
#include "switch.hpp"

// This class generates the Gray code for (n+1,n+1)-combinations by star transpositions
// for the parameters n and s, and it call the user-supplied visit function on each generated
// bitstring. All the work happens in the constructor. After the constructor has completed
// its work, the object can be used to retrieve some statistics (e.g., the length of the
// generated Gray code), but should not be used otherwise.
class Knuth_gray_code {
  friend class Tests;

private:
  const int n_;
  const int s_;
  std::vector<bool> x_;     // current bitstring (including the first bit that alternates in each step)
  std::vector<bool> start_; // starting bitstring without the first alternating bit
  // internally, the algorithm works with a scaled version of the bitstrings and the shift
  int s_scaled_;
  std::vector<bool> x_scaled_;
  std::vector<bool> start_scaled_;
  int fac_;  // the scaling factors to use in both directions x_ * fac_ = xp_
  int fac_inv_;

  long long steps_;         // upper bound on the number of bistrings that the algorithm will visit
                            // -1 is whole cycle
  long long current_step_;
  std::function<void (int, const std::vector<bool> &)> visit_;  // user-supplied function called on every generated string

  // We either use the class Periodic_path (the actual algorithm) to generate the code for n>=4,
  // or the class Small_code, which provides hardcoded flip sequences for n=1,2,3. The other unused
  // variable will be in some invalid state.
  bool use_hardcoded_;

  Periodic_path path_;
  std::vector<Switch> switches_;  // list of usable switches for the given n; non-empty only for 4<=n<=38.
  int shift_change_;   // how much we must change the default shift value Catalan(n) to get the desired shift s_ (relevant only for 4<=n<=38)

  Small_code small_;

public:
  // generate the Gray code for given n and shift s, starting from given initial bitstring start
  Knuth_gray_code(int n, int s, const std::vector<bool> &start, long long steps, std::function<void (int, const std::vector<bool> &)>);

  // return number of visited bitstrings
  inline int get_visited_count() { return current_step_; }

private:
  // flip i-th bit of x
  inline void flip(std::vector<bool> &x, const int i) { x[i] = !x[i]; }

  // scale x_ by the given factor to obtain x_scaled_
  void scale(int fac);

  // initialize scaling factors and switches, the argument specifies the shift value obtained when applying no switches
  void init_switches(int shift);
  // check if we are currently sitting on the given switch
  bool are_on_switch(Switch &swi);  // ("switch" is a reserved keyword)
  // Check if there is a usable switch in switches_ that we are currently sitting on, and return it.
  // If the first part of the result is false, then there is no usable switch and second part of the result can be ignored.
  std::pair<bool, Switch> get_usable_switch();

  bool reached_end();
};

#endif