/*
 * Copyright (c) 2016 Petr Gregor, Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CYCLE_HPP
#define CYCLE_HPP

#include <cassert>
#include <vector>

// An abstract class that is used to model a trimmed Gray code, a saturated cycle
// or a tight enumeration in the cube (also a Hamilton cycle in the middle levels graph).
// The class is templated in the vertex type (which can be e.g. std::vector<int> or
// Vertex in the middle levels code).

// type of user-defined visit function
// input arguments are the current vertex x and the position
// that has been flipped in the last step (to reach x)
typedef void (*visit_f_t)(const std::vector<int>& x, const std::vector<int>& pos);

template <typename VertexT> class AbstractCycle {
public:
  explicit AbstractCycle(int n, long long limit, visit_f_t visit_f) : n_(n), limit_(limit), visit_f_(visit_f) { }

  // get first vertex (this function has to be implemented
  // in each derived class depending on the vertex type)
  virtual const std::vector<int>& get_first_vertex() const = 0;
  // get the number of vertices generated along the cycle
  long long get_length() const { return length_; }

  // Two functions called by the middle levels algorithm HamCycle()
  // to log its behavior from the outside.
  // This will be needed both in the SatCycle class and also in
  // the TightEnum class (both need a Hamilton cycle in the middle
  // levels graph as a special case).
  virtual void visit_f_visit(int j) = 0;
  virtual void visit_f_log(int j) = 0;

protected:
  VertexT x0_;  // the first vertex
  VertexT x_;  // the current vertex
  int n_;  // the dimension of the cube
  long long limit_;  // the number of vertices to be generated
  visit_f_t visit_f_;  // user-defined visit function
  long long length_;  // number of vertices generated so far

  // #### auxiliary functions ####

  // test whether given vertex is
  // a_{n,k} = 0^{n-k}1^{k} or its complement
  bool is_a_vertex(const std::vector<int>& x, bool complement, int n, int k) const {
    int zero_bit = complement ? 1 : 0;
    int one_bit = complement ? 0 : 1;
    for (int i = 0; i < n; ++i) {
      if (((i < n - k) && (x[i] == one_bit)) || ((i >= n - k) && (x[i] == zero_bit))) {
        return false;
      }
    }
    return true;
  }

  // test whether given vertex is
  // b_{n,k} = 0^{n-k-1}1^{k}0 or its complement
  bool is_b_vertex(const std::vector<int>& x, bool complement, int n, int k) const {
    int zero_bit = complement ? 1 : 0;
    int one_bit = complement ? 0 : 1;
    for (int i = 0; i < n; ++i) {
      if ((((i < n - k - 1) || (i == n - 1)) && (x[i] == one_bit)) || (((i >= n - k - 1) && (i < n - 1)) && (x[i] == zero_bit))) {
        return false;
      }
    }
    return true;
  }

  // compute the inverse permutation
  void inverse_permutation(const std::vector<int>& pi, std::vector<int>& inv) const {
    int n = pi.size();
    inv.resize(n);
    for (int i = 0; i < n; ++i) {
      inv[pi[i]] = i;
    }
  }

  // compute the product of two permutations (of possibly different lengths, pi1.size() >= pi2.size())
  // prod[i] = (pi1 * pi2)[i] = pi1[pi2[i]]
  void product_permutation(const std::vector<int>& pi1, const std::vector<int>& pi2, std::vector<int>& prod) const {
    assert(pi1.size() >= pi2.size());
    prod.resize(pi2.size());
    for (int i = 0; i < pi2.size(); ++i) {
      prod[i] = pi1[pi2[i]];
    }
  }

  // apply given permutation pi to given bitstring x
  void permute(const std::vector<int>& pi, std::vector<int>& x) const {
    assert(x.size() >= pi.size());
    std::vector<int> pi_inv;
    inverse_permutation(pi, pi_inv);
    for (int i = 0; i < pi_inv.size(); ++i) {
      x[i] = x[pi_inv[i]];
    }
    // remaining entries of x remain unchanged
  }

  // define the identity permutation pi=(0,1,\ldots,n-1)
  void id_permutation(int n, std::vector<int>& id) const {
    id.resize(n);
    for (int i = 0; i < n; i++) {
      id[i] = i;
    }
  }
};

#endif
