#!/usr/bin/perl

# Utility perl routines for the Object Server.
# Written by Frank Ruskey, 1995, 1996.

require "../cgi-lib.pl";

select((select(STDOUT), $| = 1)[$[]);
select((select(STDERR), $| = 1)[$[]);

$BinDir = "/bin";
$htmldocs = "inf";
$icons = "ico";
$binaries = "/theory/www/bin";
$LIMIT = 1000;
$LIMITerror = -1;

sub ErrorHeader {
   print "<BR><BLINK><B><FONT COLOR=FF0000>Oops:</FONT></B></BLINK> ";
}

sub ErrorTrailer {
   print "<P><A HREF=$ENV{HTTP_REFERER}>";
   print "<IMG SRC=ico/gen.gif align=left border=0> Return?</A>\n";
}

sub BadOutput {
	&ErrorHeader;
	print "Output type is not applicable.\n";
	&ErrorTrailer;
}

# Issue an error message 
sub ErrorMessage {
   &ErrorHeader;
   print "$_[0]<BR>\n";
   &ErrorTrailer;
 }

# Too may objects requested.  Follows each call to generate something.
sub LimitError {
   &ErrorHeader;
   print "You asked for more objects than the current limit of $LIMIT.<BR>\n";
   &ErrorTrailer;
}

# An extra parameter has been supplied.  It will be ignored.
sub ExtraParam {
   print "<FONT SIZE=-1>Parameter <I>$_[0]</I> = $_[1] ignored.</FONT><BR>\n";
}

sub OutputIgnored {
   print "<FONT SIZE=-1>Output parameter <I>$_[0]</I> ignored.</FONT><BR>\n";
}

sub Cleanup {
   MoreInfo($_[0]);
   print "</Body></Html>\n";
   exit(0);
}

sub MoreInfo{
   print "<P><HR><A HREF=\"$htmldocs/$_[0].html\">";
   print "<IMG SRC=ico/question.gif align=left border=0></A>\n";
   &PrintBorder;
}

sub WorkingMoreInfo{
   $number = @_;
   if($number == 2){
	print "<P><HR><A HREF=\"$htmldocs/$_[0].html#$_[1]\">";}
   else{
	print "<P><HR><A HREF=\"$htmldocs/$_[0].html\">";}
   print "<IMG SRC=ico/question.gif align=left border=0></A>\n";
   &PrintBorder;
}

# Error message &TooBig( x, y ) means that variable named x is bigger
# than value y, but shouldn't be.
sub TooBig {
   &ErrorHeader;
   print "Parameter <I>$_[0]</I> cannot be larger than $_[1].<BR>\n";
   &ErrorTrailer;
}

# Error message &TooSmall( x, y) means that variable named x is smaller
# than value y, but shouln't be.
sub TooSmall {
   &ErrorHeader;
   print "Parameter <I>$_[0]</I> cannot be smaller than $_[1].<BR>\n";
   &ErrorTrailer;
}

# Error message.
sub MustBeEven {
   &ErrorHeader;
   print "Parameter <I>$_[0]</I> must be even.<BR>\n";
   &ErrorTrailer;
}

# Error message.
sub MustBeOdd {
   &ErrorHeader;
   print "Parameter <I>$_[0]</I> must be odd.<BR>\n";
   &ErrorTrailer;
}

sub PrintBorder {
   print '<A HREF="inf/info.html"><IMG ALIGN=right border=0 SRC=ico/objinfo.gif></A>';
   print "\n<A HREF=cos.html><IMG ALIGN=right border=0 SRC=ico/objgen.gif></A>\n";
   print "<A HREF=root.html><IMG ALIGN=right border=0 SRC=ico/cos.gif></A>\n";
   print "<BR clear=all>";
}

# GOOGLE ANALYTICS
sub GoogleStuff {
   print "\n<!-- GOOGLE ANALYTICS -->\n"; 
   print "<script type=\"text/javascript\">\n";
   print 'var _gaq = _gaq || [];';
   print '    _gaq.push([\'_setAccount\', \'UA-23538709-1\']);';
   print '    _gaq.push([\'_trackPageview\']);';

   print '    (function() {';
   print '       var ga = document.createElement(\'script\'); ga.type = \'text/javascript\'; ga.async = true;';
   print '       ga.src = (\'https:\' == document.location.protocol ? \'https://ssl\' : \'http://www\') + \'.google-analytics.com/ga.js\';';
   print '       var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(ga, s);';
   print '    })();';

   print "\n</script>\n";
}


# Every COS output file should start with this.
sub OutputHeader {
   print &PrintHeader;
   print '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">';
   print "\n";
   print "<HTML><HEAD>\n";  # ?? should this go earlier?
   print '<META HTTP-EQUIV="Reply-to" CONTENT="fruskey@csr.uvic.ca">';
   print "\n";
   print "<TITLE>$_[0]</TITLE>\n";
   print '<BASE HREF="http://theory.cs.uvic.ca/">';
   &GoogleStuff;
   print "</HEAD>\n<BODY>\n";
   print "<A HREF=\"inf/$_[1].html\">";
   print "<IMG SRC=ico/question.gif align=left border=0></A>";
   print "\n";
   &PrintBorder;
   print "<HR>\n";
   print "<H3>$_[0]</H3>\n";  # ?? Why H3?
}

# Format of output table.
sub TableSetup {
   print "<TABLE BORDER=1 CELLSPACING=2 CELLPADDING=2><TR>\n";
}

# Output a column header (E.g., &ColumnLabel( "Cycle" ) ).
sub ColumnLabel {
   print '<TH COLSPAN=1><FONT SIZE="+1">';
   print "$_[0]";
   print "</FONT><BR></TH>\n";
}

# Every COS document should end with this.
sub CloseDocument {
   # &PrintBorder;
   print "</BODY></HTML>\n";
   print "<!-- Object Server Output -->\n";
   print "<!-- Copyright, Frank Ruskey, 1995-1997. -->\n";
}

# Strips out all non-numeric characters.  No warning issued.
# For security reasons a call to "MakeNumeric" should precede 
# each use of a numeric input.
sub MakeNumeric {
   $_[0] =~ s/[^0-9]//g;
}

1; # return true

