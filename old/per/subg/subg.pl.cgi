#!/usr/bin/perl  
# !/usr/bin/perl5 -w

require "../common.pl";

$BinDir = "../../bin/subg";
$MaxN   = 100;

&InitIO;

MAIN: 
{
  if (&ReadParse(*input)) {
    &ProcessForm;
  }
}

sub ProcessForm {

  &OutputHeader( 'Subgraph Output', 'subg/Spanning' );
  if ($input{'output1'}) { $outformat = 1;}
  if ($input{'output2'}) { $outformat = $outformat | 2;}

  if ($input{'program'} eq "Spanning") {
     if ($outformat eq "") {
        &ErrorMessage('The output has not been specified or is not applicable');
        Cleanup('subg/Spanning');
     }
     if (int($input{'n'}) > $MaxN) { 
	&TooBig('n',$MaxN);  
	Cleanup('subg/Spanning');
     }
     &parseEdges;
     print "Spanning Trees.<BR>\n";
     $retval = system "$BinDir/Spanning < ./graph.temp | /usr/bin/perl format.pl $outformat $input{'n'}";	
     if ($retval) { &LimitError; }
  } 

  elsif ($input{'program'} eq "Cutset") {
     &ErrorMessage( 'This option not yet implemented.' );
  }

  elsif ($input{'program'} eq "Cycle") {
     &ErrorMessage( 'This option not yet implemented.' );
  }

  elsif ($input{'program'} eq "Acyclic") {
     &ErrorMessage( 'This option not yet implemented.' );
  }

  &WorkingMoreInfo( 'subg/Spanning' );
  print "</BODY></HTML>\n";
}

###########################################################################
#The following was modified by Joe Sawada from the code written by
#Jeremy Schwartzentruber (jschwart@csr.uvic.ca)
#for the linear extensions CGI  - poset.pl.cgi
###########################################################################

sub InitIO {
  $oldhandle = select(STDOUT);
  $| = 1;
  select($oldhandle);

  $oldhandle = select(STDERR);
  $| = 1;
  select($oldhandle);
}

sub gettoke {
  local($toke);
  if ($firsttime) {
     $nextline = $input{'edges'};
     $firsttime = 0;
  }
  if (!$nextline) {
     return '';
  } else {
     ($toke) = $nextline =~ /^\s*(\{|>|\-|<|\}|\,|[^<>\-\{\{\}\,\s]+)\s*/;
     $nextline = $';
     return $toke;
  }
}

sub store {
  local($first, $second) = @_;
  if ($first > $n || $second > $n) {
     &ErrorMessage('All nodes must be less than or equal to N');
     &WorkingMoreInfo( 'pose/LinearExt' );
      exit;
  }
  print FILE "$first $second\n";
}

sub startEdges {
  $file = "./graph.temp";
  open(FILE, ">$file") || die "Can't open $file:$!\n";
  print FILE "$n\n";
  $firstint='';
  $secondint='';
  $lastint='';
  $newpair=1;
}

sub finishpair {
	$secondint=$lastint;
	&store($firstint, $secondint);
	$firstint='';
	$secondint='';
	$lastint='';
	$newpair=1;
}

sub finishEdges {
	print FILE "0 0\n";
	close(FILE);
}

sub less {
	$firstint=$lastint;
}

sub symbol {
	if ($newpair) {
		($lastint) = @_;
		$newpair=0;
	} else {
		($lastint) = @_;
		&finishpair;
	}
}

sub parseEdges {
	$n = $input{'n'} + 0;
	$nextline = '';
	$firsttime = 1;

	$_ = &gettoke;
	if (/^\{$/) {
		&startEdges;
	} else {
        	&ErrorMessage('List must start with a "{"');
     		&WorkingMoreInfo( 'subg/Spanning' );
		exit;
	}
	while ($_=&gettoke) {
		PARSE: {
			if (/^\}$/) { &finishEdges; }
			elsif (/^\,$/) {
				if (!$newpair) { &finishpair; }
			}
			elsif (/^\-$/)  { 
				# Extra if statement added to check for
				# bad data of the form { 1-2, -3} 
				if ($newpair) {
        				&ErrorMessage('Bad input.');
     					&WorkingMoreInfo( 'subg/Spanning' );
					exit;
				}
				else { &less; }
			}
			else {&symbol($_); }
		}
	}
}







