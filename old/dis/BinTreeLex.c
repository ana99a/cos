/*======================================================================*/
/* C program for distribution from the Combinatorial Object             */
/* Server. Generate binary trees in lexicographic order.                */
/* This is the same version used in the book "Combinatorial Generation."*/
/* The program can be modified, translated to other languages, etc.,    */
/* so long as proper acknowledgement is given (author and source).      */  
/* Programmer: Joe Sawada, 1997.                                        */
/* The latest version of this program may be found at the site          */
/* http://theory.cs.uvic.ca/inf/tree/BinaryTrees.html                   	*/
/*======================================================================*/

#include <stdio.h>


int a[101];
int N;

void printTree() {
	
	int j;

	for(j=1; j<=2*N; j++) printf("%d",a[j]);
	printf("\n");
}

int Next() {

	int k,q,p,j;

	
	k= 2*N;
	while (a[k] == 0) k--;
	q= 2*N -k;
	while (a[k] == 1) k--;
	if (k==0) return(0); 
	else { 
		p= 2*N -k -q;
		a[k]=1;
		for(j=k+1; j<=k+q-p+2; j++) a[j]=0;
		j=2*N - 2*p + 3;
		while (j<=2*N - 1) { 
			a[j]= 1; 
			a[j+1]= 0; 
			j= j+2;
	 	}	
		printTree(); 
	}	
	return(1);
}

void main() {

	int i;	

	printf("Enter N: ");
	scanf("%d",&N);
	if (N<1) printf("N must be greater than 0.\n\n");
	else { 
		a[0] = 0;
		for (i=1; i<=2*N; i++)  a[i] = i%2;
		printf("\n");
		printTree();
		while (Next()) {}
		printf("\n");
	}
}
			
			
