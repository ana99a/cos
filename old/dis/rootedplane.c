/*==============================================================================*/
/* Program: Generating plane rooted trees        				*/
/* Input: n                                      				*/
/* Output: All tree sequences with n nodes       				*/
/* Programmer: Lara Wear                         				*/
/* Algorithm:  Joe Sawada, University of Victoria				*/
/*	       March 22, 2002			 				*/
/* The program can be modified, translated to other languages, etc., so long as */
/* proper acknowledgement is given (author and source). 			*/
/* The latest version of this program may be found at the site	         	*/
/* http://www.theory.csc.uvic.ca/inf/tree/PlaneRootedTree.html		*/
/*==============================================================================*/

#include <stdlib.h>
#include <stdio.h>

#define MAX 50   /* max # nodes */
static int a[MAX];
static int counter = 0;
int N; /* # nodes */

void printIt();
void RootedPlane (int t, int p);

int main()
{
      /* variables */
      int i;

      /* N = # nodes */
      printf("Enter the number of nodes: ");
      scanf("%d", &N);

      /* initialize array a to 0111... */
      a[0] = 0;
      for (i = 1; i < N; i++)
          a[i] = 1;
      if (N == 1) { printf("0\n"); counter = 1;}
      else RootedPlane(2,1);
      printf("Total number of trees: %d", counter);
      return 0;
} /* main */

/* RootedPlane - generates rooted plane trees with N nodes
 * Algorithm from ....
 * inputs: t - # nodes, p - # trees for t nodes
 * outputs: none
*/

void RootedPlane (int t, int p) {
     int j;
     if (t == N) {
        if ( ((N-1)%p) == 0 ) printIt();
     }  
     else {
         for (j = a[t-p]; j <= a[t-1] + 1; j++) {
             a[t] = j;
             if (j == a[t-p])
                RootedPlane(t+1,p);
             else
                RootedPlane(t+1, t);
         } 
     } 
} /* RootedPlane */


void printIt () {
     int i;
     counter++;
     for (i=0; i < N; i++) {
         printf("%2d",a[i]);
     } /* for */
     printf("\n");
} 
