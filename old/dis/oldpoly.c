/*===================================================================*/
/* C program for distribution from the Combinatorial Object Server.  */
/* Program to enumerate all irreducible and/or primitive polynomials */
/* over GF(2).  This is the same version described in the book       */
/* "Combinatorial Generation", Frank Ruskey, to appear.              */
/* The program can be modified, translated to other languages, etc., */
/* so long as proper acknowledgement is given (author and source).   */
/* Not to be used for commercial purposes without prior consent.     */
/* The latest version of this program may be found at the site       */
/* http://theory.cs.uvic.ca/inf/neck/Polynomial.html                  */
/* Copyright 1997,1998 F. Ruskey and K. Cattell                      */
/*===================================================================*/
                                                                            
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* set this to your machine word size (sizeof(unsigned long)*8) */
#define MAX 32
typedef unsigned long ulong;

/* globals */
int n;
ulong twonm1;  /* 2^n-1 */
ulong checked = 0; /* nuber checked so far */
ulong printed = 0; /* number printed so far */
ulong doneFlag;    /* for early exit */

/* run paramaters */
int maxDensity;
int type;
int outformat;
ulong number;
ulong iter_limit;

/* stats */
ulong ir_count = 0, pr_count = 0, density; 
ulong ir_hist[MAX], pr_hist[MAX];
ulong prim_poly;

/* the necklace */
int b[MAX+1];

struct pair {
  ulong poly;
  ulong pow2m1;
};

struct pair poly_table[MAX+1] = {  /* prime factorization of 2^n-1 */
  /*  0 */  {  1,          0 },  /* (0)                               */
  /*  1 */  {  1,          1 },  /* (1)                               */
  /*  2 */  {  3,          3 },  /* (3)          it's prime!          */
  /*  3 */  {  3,          7 },  /* (7)          it's prime!          */
  /*  4 */  {  3,         15 },  /* (3) (5)                           */
  /*  5 */  {  5,         31 },  /* (31)         it's prime!          */
  /*  6 */  {  3,         63 },  /* (3) (3) (7)                       */
  /*  7 */  {  3,        127 },  /* (127)        it's prime!          */
  /*  8 */  { 29,        255 },  /* (3) (5) (17)                      */
  /*  9 */  { 17,        511 },  /* (7) (73)                          */
  /* 10 */  {  9,       1023 },  /* (3) (11) (31)                     */
  /* 11 */  {  5,       2047 },  /* (23) (89)                         */
  /* 12 */  { 83,       4095 },  /* (3) (3) (5) (7) (13)              */
  /* 13 */  { 27,       8191 },  /* (8191)       it's prime!          */
  /* 14 */  { 43,      16383 },  /* (3) (43) (127)                    */
  /* 15 */  {  3,      32767 },  /* (7) (31) (151)                    */
  /* 16 */  { 45,      65535 },  /* (3) (5) (17) (257)                */
  /* 17 */  {  9,     131071 },  /* (131071)     it's prime!          */
  /* 18 */  { 39,     262143 },  /* (3) (3) (7) (19) (73)             */
  /* 19 */  { 39,     524287 },  /* (524287)     it's prime!          */
  /* 20 */  {  9,    1048575 },  /* (3) (5) (5) (11) (31) (41)        */
  /* 21 */  {  5,    2097151 },  /* (7) (7) (127) (337)               */
  /* 22 */  {  3,    4194303 },  /* (3) (23) (89) (683)               */
  /* 23 */  { 33,    8388607 },  /* (47) (178481)                     */
  /* 24 */  { 27,   16777215 },  /* (3) (3) (5) (7) (13) (17) (241)   */
  /* 25 */  {  9,   33554431 },  /* (31) (601) (1801)                 */
  /* 26 */  { 71,   67108863 },  /* (3) (8191) (2731)                 */
  /* 27 */  { 39,  134217727 },  /* (7) (73) (262657)                 */
  /* 28 */  {  9,  268435455 },  /* (3) (5) (29) (43) (113) (127)     */
  /* 29 */  {  5,  536870911 },  /* (233) (1103) (2089)               */
  /* 30 */  { 83, 1073741823 },  /* (3) (3) (7) (11) (31) (151) (331) */
  /* 31 */  {  9, 2147483647 },  /* (2147483647) it's prime!          */
  /* 32 */  {175, 4294967295 }   /* (3) (5) (17) (257) (65537)        */
#if (MAX > 32) 
  ,
  /* 33 */  { 83, 8589934591 },  /* (7) (23) (89) (599479)            */
  /* 34 */  {231, 17179869183        },/*131071.3.43691*/
  /* 35 */  {5,   34359738367        },/*71.122921.31.127*/
  /* 36 */  {119, 68719476735        },/*7.73.3.3.19.13.5.37.109*/
  /* 37 */  {63,  137438953471       },/*616318177.223*/
  /* 38 */  {99,  274877906943       },/*524287.3.174763*/
  /* 39 */  {17,  549755813887       },/*7.8191.121369.79*/
  /* 40 */  {57,  1099511627775      },/*31.3.11.5.5.41.17.61681*/
  /* 41 */  {9,   2199023255551      },/*164511353.13367*/
  /* 42 */  {63,  4398046511103      },/*337.7.7.127.43.3.3.5419*/
  /* 43 */  {89,  8796093022207      },/*2099863.431.9719*/
  /* 44 */  {101, 17592186044415     },/*23.89.3.683.5.397.2113*/
  /* 45 */  {27,  35184372088831     },/*7.151.73.31.631.23311*/
  /* 46 */  {303, 70368744177663     },/*178481.47.3.2796203*/
  /* 47 */  {33,  140737488355327    },/*2351.13264529.4513*/
  /* 48 */  {183, 281474976710655    },/*7.3.3.13.5.17.241.257.97.673*/
  /* 49 */  {113, 562949953421311    },/*127.4432676798593*/
  /* 50 */  {29,  1125899906842623   },/*601.1801.31.3.11.251.4051*/
  /* 51 */  {75,  2251799813685247   },/*7.131071.11119.2143.103*/
  /* 52 */  {9,   4503599627370495   },/*8191.3.2731.5.53.157.1613*/
  /* 53 */  {71,  9007199254740991   },/*20394401.6361.69431*/
  /* 54 */  {125, 18014398509481983  },/*7.262657.73.19.3.3.3.3.87211*/
  /* 55 */  {71,  36028797018963967  },/*881.23.89.31.3191.201961*/
  /* 56 */  {149, 72057594037927935  },/*127.43.3.29.113.5.17.15790321*/
  /* 57 */  {45,  144115188075855871 },/*7.524287.1212847.32377*/
  /* 58 */  {99,  288230376151711743 },/*2089.233.1103.3.59.3033169*/
  /* 59 */  {123, 576460752303423487 },/*179951.3203431780337*/
  /* 60 */  { 3,  1152921504606846975},/*7.151.31.3.3.11.331.13.5.5.41.61.1321*/
  /* 61 */  {39,  2305843009213693951   },/*2305843009213693951*/
  /* 62 */  {105, 4611686018427387903   },/*2147483647.3.715827883*/
  /* 63,*/  {3 ,  9223372036854775807   },/*337.7.7.73.127.649657.92737*/
  /* 64 */  {27,  18446744073709551615  } /*3.5.17.257.65537.641.6700417*/
#endif
};

void ProcessInput (int argc, char *argv[])
{ 
  outformat = 0;
  type = 0;
  number = 0;
  iter_limit = 0;
  maxDensity = 0;

  if (argc > 1 && argc <= 7) {
    n = atoi(argv[1]);
    if (argc > 2) outformat = atoi(argv[2]);
    if (argc > 3) type = atoi(argv[3]); 
    if (argc > 4) number = atoi(argv[4]);
    if (argc > 5) iter_limit = atoi(argv[5]);
    if (argc > 6) maxDensity = atoi(argv[6]);
  } 
  else {
    fprintf(stderr,"Usage: poly n format type number iterlimit density\n");
    fprintf(stderr,"         All params except 'n' are optional\n");
    fprintf(stderr,"         n = degree\n");
    fprintf(stderr,"         format = 0,1,2 = string,ceoffs,poly\n");
    fprintf(stderr,"         type = 0/1 = all/primitive only\n");
    fprintf(stderr,"         num = max to find (0 for all)\n");
    fprintf(stderr,"         iter_limit = max to check (0 for all)\n");
    fprintf(stderr,"         d = max density (0 for all)\n");
    exit(1);
  }
  printf( "degree      = %d\n", n );
  printf( "format      = %d\n", outformat );
  printf( "type        = %s\n", type ? "primitive" : "all" );
  printf( "num         = %d %s\n", number, number ? "" : "(none)" );
  printf( "iterlimit   = %d %s\n", iter_limit, iter_limit ? "" : "(none)" );
  printf( "max density = %d %s\n", maxDensity, maxDensity ? "" : "(none)" );
}

/* for checking primitivity */
ulong gcd ( ulong n, ulong m )
{
  if (m == 0) return(n); 
  else return(gcd(m,n%m));
}

void PrintString( int n, ulong a) {
  int i;
  printf("1 ");  
  for ( i=n-1; i>=0; i-- )
    if ( a & (1<<i) ) {
      if (i != 0) printf("1 "); 
    }
    else printf("0 ");
  printf("1\n"); 
}
void PrintPoly( int n, ulong a) {
  int i;
  density = 1;
  printf("x^%d + ", n );  
  for ( i=n-1; i>=0; i-- )
    if ( a & (1<<i) ) {
      ++density;
      if (i != 0) printf("x^%d + ", i ); 
   }
  printf("1\n"); 
}

void polyToString( int n, ulong a, char* s )
{
  int i;
  density = 1;
  sprintf( s, "%d\0", n );  
  for ( i=n-1; i>=0; i-- )
    if ( a & (1<<i) ) {
      ++density;
      sprintf( s+strlen(s), ", %d\0", i ); 
    }
  ++ir_hist[density];
}


ulong multmod( int n, ulong a, ulong b, ulong p )
{
  ulong t, rslt;
  ulong top_bit;
  int i;

  rslt = 0;
  t = a; /* t is a*x^i */
  top_bit = 1 << (n-1);

  for ( i=0; i<n; i++ )
  {
     if (b & 1) rslt ^= t;
     if (t & top_bit)
        t = ( (t & ~top_bit) << 1 ) ^ p;
     else
        t = t << 1;
     b = b >> 1;
  }
  return rslt;
}

ulong powmod( int n, ulong a, ulong power, ulong p )
{
  ulong t = a;
  ulong rslt = 1;
  while ( power != 0 )
  {
    if ( power & 1 ) rslt = multmod( n, t, rslt, p );
    t = multmod( n, t, t, p );
    power = power >> 1;
  }
  return rslt;
}

ulong minpoly( int n, ulong necklace, ulong p )
{
  ulong root, rslt = 0;
  ulong f[ MAX ];
  int i, j;

  f[0] = 1;
  for (i=1; i<n; i++ ) f[i] = 0;

  root = powmod( n, 2, necklace, p ); /* '2' is monomial x */
  for (i=1; i<=n; i++ )
  {
    if (i != 1)
      root = multmod( n, root, root, p );

    for (j=n-1; j>=1; j--)
      f[j] = f[j-1] ^ multmod( n, f[j], root, p );
    f[0] = multmod( n, f[j], root, p );
  }

  for (i=0; i<n; i++ )
    if (f[i] == 1)
      rslt |= 1 << i;
    else if (f[i] != 0)
      fprintf( stderr, "Ahh!" );

  return rslt;
}

ulong toInt ( )
{
ulong i,x;
  x = 0;
  for (i=1; i<=n; ++i) {
     x = 2*x;
     if (b[i] == 1) ++x;
  }
  return( x );
}
 

void PrintIt( ulong p )
{
  ulong necklace;
  ulong m;
  int i;
  char s[100];
  static int count = 0;

  if (p != n) return; 

  if (iter_limit && checked++ >= iter_limit) { 
    printf("Iteration limit exceeded !!\n");
    doneFlag = 1;
    return;
  }

  necklace = toInt();
  m = minpoly( n, necklace, prim_poly );
  density = 1;
  for ( i=n-1; i>=0; i-- )
    if ( m & (1<<i) ) ++density;
  if (maxDensity && density > maxDensity) return;

  ++ir_count;
  
  polyToString( n, m, s ); 
  /* check if it's primitive */
  if ( gcd( necklace, twonm1 ) == 1 ) {
    ++pr_count;
    if (outformat == 0) PrintString(n,m); 
    if (outformat == 1) printf("%s\n",s);  
    if (outformat == 2) PrintPoly(n,m); 
    printed++;
  }
  /* print only if type = 0 (irreducible) */
  else if (type == 0) {
    if (outformat == 0) PrintString(n,m); 
    if (outformat == 1) printf("%s\n",s);  
    if (outformat == 2) PrintPoly(n,m); 
    printed++;
  }
  if (number && printed >= number)
  {
    printf("Print limit exceeded !!\n");
    doneFlag = 1;
  }
}

void gen( ulong t, ulong lyn )
{
  if (t > n) 
    PrintIt( lyn );
  else {
    b[t] = b[t-lyn];  
    gen( t+1, lyn );
    if (doneFlag) return;
    if ((b[t-lyn] == 0) && (n>1)) {
      b[t] = 1;  
      gen( t+1, t );
      if (doneFlag) return;
    }
  }
}

int main (int argc, char *argv[] )
{
  int i;
  ProcessInput( argc, argv );

  for(i=0; i<=n; ++i) ir_hist[i] = 0;
  prim_poly = poly_table[n].poly;
  twonm1 = poly_table[n].pow2m1; 
  b[0] = 0;
  gen(1,1);

  if (type == 0) {
    printf( "The number of irreducible polynomials = %d\n", ir_count);
  }
  printf( "The number of primitive polynomials   = %d\n", pr_count );
  return(0);
} 

