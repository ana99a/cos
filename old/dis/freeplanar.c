/*==============================================================================*/
/* Program: Generating free plane trees          				*/
/* Input: n                                      				*/
/* Output: Level sequences with n nodes          				*/
/* Programmer: Lara Wear                         				*/
/* Algorithm:  Joe Sawada, University of Victoria				*/
/*	       March 22, 2002			             			*/
/* The program can be modified, translated to other languages, etc., so long as */
/* proper acknowledgement is given (author and source). 			*/
/* The latest version of this program may be found at the site	         	*/
/* http://www.theory.csc.uvic.ca/inf/tree/PlaneFreeTree.html		*/
/*==============================================================================*/

#include <stdlib.h>
#include <math.h>

#define MAX 50   /* max # nodes */
#define FALSE 0
#define TRUE 1

static int a[MAX];
static int counter = 0;
int N; /* # nodes */

typedef char boolean;

void printIt();
void unicentroid (int t, int p, int s);
void bicentroid (int t, boolean flag);

int main()
{
      /* variables */
      int i;

      /* N = # nodes */
      printf("Enter the number of nodes: ");
      scanf("%d", &N);

      /* initialize array a to 0100... */
      a[0] = 0;  a[1] = 1;
      for (i = 2; i < N; i++) a[i] = 1;

      /* for all values of N, generate unicentroidal trees */
      if (N == 1) { printf("0\n"); counter = 1;}
      else unicentroid(2,1,1);

      /* for N even, also generate bicentroidal trees */
      if (!(N%2)) bicentroid(1,0);

      printf( "There are %d trees.", counter);

      return 0;
} /* main */

/* unicentroid - Generates unicentroidal trees with N nodes
 * Algorithm: Joe Sawada, University of Victoria
 * Inputs: t - # nodes, p - # trees for t nodes, s - size of current subtree from root
 * Outputs: none
*/
void unicentroid(int t, int p, int s) {
     int max;
     int j;
     if (t == N ) {
         if ((N-1)%p == 0) printIt();
     }
     else {
        if (s == (N-1)/2) {
          max = 1;
        }
        else
           max = a[t-1]+1;
        for (j = a[t-p]; j <= max; j++) {
            a[t] = j;
            if (j == a[t-p] && j == 1)
               unicentroid(t+1, p, 1);
            else if (j == a[t-p])
               unicentroid(t+1, p, s+1);
            else
               unicentroid(t+1, t, s+1);
        } /* for */
     } /*else */

} /* unicentroid */


/* bicentroid - Generates bicentroidal trees with N nodes (N even)
 * Algorithm: Joe Sawada, University of Victoria
 * Inputs: t - # nodes, flag - boolean flag
 * Outputs: none
*/

void bicentroid(int t, boolean flag) {
     int j;
     int min;
     if (t == N) printIt();
     else if (t == N/2) {
          a[t] = 1;
          bicentroid(t+1, 1);
     }
     else if (!flag) {
          if (t > N/2) min = 2;
          else min = 1;
          for (j = min; j <= a[t-1]+1; j++) {
              a[t] = j;
              bicentroid(t+1, 0);
          } /* for */
     }
     else {
          for (j = a[t-N/2]+1; j <= a[t-1]+1; j++) {
              a[t] = j;
              if (j == a[t-N/2] + 1) bicentroid (t+1,1);
              else bicentroid(t+1, 0);
          } /* for */
     } /* else */
} /* bicentroidal */


/* Print tree */
void printIt () {
     int i;
     counter++;

     for (i=0; i < N; i++) {
         printf("%d ", a[i]);
     } /* for */
     printf("\n");
} /* printIt */
