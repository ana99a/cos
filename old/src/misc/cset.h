#ifndef _CSET_H
#define _CSET_H

//========================================================================
// By:  John Boyer
// On:  19 SEP 1995
//
// This module implements a class called CSet that provides a basic
// ability to manipulate sets, including set membership, intersection,
// union, and complement.
//
// A set containing N elements will be represented by this class as the
// subset of natural numbers given by {0, ..., N-1}.

// Updated:  17 NOV 1995
//	Added functions GetLeastElement() and GetGreatestElement()
//            as well as operator -=
//========================================================================

#include <iostream.h>

#define OK	0
#define NOTOK	-1

// ASSUMPTION:  An unsigned char must contain 8 bits; if not, then your
//              compiler outdates this source code.  You must redefine
//              this type definition to use a type that provides an
//              unsigned 8 bit memory space.

typedef unsigned char byte;

class CSet
{
     byte *Members;
     int MaxSize, Status;

     void DestroySet();

protected:

     virtual int Stopper(int Current, int Iterator);

public:

     CSet(int theMaxSize, int Initializer=0);
     void Initialize(int theMaxSize, int Initializer=0);
     CSet();
     CSet(CSet&);
     ~CSet();

     int  GetStatus() { return Status; };
     int  GetSize() { return MaxSize; };
     int  GetLeastElement(int N=0);
     int  GetGreatestElement(int N=-1);
     int  IsEmpty();
     int  IsFull();
     void FillSet(int Start, int Iterator);

     void operator =(CSet& RHS);    // Assign RHS to this

     void operator &=(CSet& RHS);   // this intersect RHS
     void operator |=(CSet& RHS);   // this union RHS
     void operator --();            // invert this
     void operator -=(CSet& RHS);   // Remove elements of RHS from this

     int  operator <(CSet& RHS);    // Is this a proper subset of RHS?
     int  operator <=(CSet& RHS);   // Is this a subset of RHS?
     int  operator ==(CSet& RHS);   // Do this and RHS contain exactly the
                                    //    same elements?  

     void operator <<=(int Member); // Put Member into this.
     void operator >>=(int Member); // Take Member out of this.

     friend int operator <<(int Member, CSet& S); // Is Member in S?

     friend ostream& operator <<(ostream&, CSet& OSet); // output OSet
     friend istream& operator >>(istream&, CSet& ISet); // input ISet
};

#endif

