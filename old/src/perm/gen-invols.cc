// Generate involutions
// Modified to accept command line arguments

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "ProcessInput.c"
#include "commonio.h"

void swap(int, int);
void cycle(int);
int NN, KK; 
int main(int argc, char **argv)
{
	int i;
	char  choice;
	if (argc > 1) {
	    ProcessInput(argc, argv);
            NN=N; KK=K;
	    calls = 0;
	    for(i=0; i < NN; i++) Pi[i] = i;
	    cycle(0);
	}
	printf("</TABLE>");
	printf("<p><b> Involutions = %d </b></p>",count);
	
}

void Swap(int i, int j) {
    int temp;
    temp = Pi[i]; Pi[i] = Pi[j]; Pi[j] = temp;
}


void cycle(int n)
{
	int i; 
	calls++;
	if (n >= NN) { // Assuming algorithm is BEST (leap of faith?)
		PrintIt();
		}
	else {
		// Do the 1 cycle part of the recurrence
		cycle(n+1);
		
		// See if we can process 2-cycles
		if (NN-n >=2) {
			// Make a 2 cycle...
			Swap(n, n+1);
			cycle(n+2);
			// Loop through and replace ....
			for(i=n-1;i >=0; i--) {
				if (Pi[i]==i) {	// We're swapping out a singleton
					Swap(i, n); Swap (n, n+1);
					cycle(n+2);
					Swap(n, n+1); Swap(i, n);
				 }
				 else 
					{
					// We're swapping 2-cycles
					Swap(Pi[i], n); Swap(i, n+1);
					cycle(n+2);
					Swap(i, n+1); Swap(Pi[i],n);
				    }
			}
			Swap(n, n+1);
		}
	}
}



#include "commonio.cc"
	
    
