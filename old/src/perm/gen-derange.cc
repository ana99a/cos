// gen-derange.cc
//
// Generate all derangements of [n] with k cycles.
// Command line version only.
//
// Translated directly from colex-derange.p
//
// Revision History (Please Update)
//
// DATE(mmm dd yyyy)  WHAT                                             WHO
// ------------------------------------------------------------------------
//
//      JUL 01 1995   Translated from Pascal                           Udi
//      JUL ?? 1995   Added HTML tags for output                       JS
//      AUG 30 1995   Added Revision history and modified HTML tags    Udi
//      AUG 30 1995   Added check for MAX number of objects            Udi
//      AUG 31 1995   Added option to print in cycle notation          Udi
//
 
 
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include "ProcessInput.c"
#include "commonio.h"
 
void swap(int, int);
void derange_k(int, int);
void derange(int);

int NN, KK;

int main(int argc, char *argv[])
{
        int i;
        
        ProcessInput(argc, argv);
        calls = 0;
        NN = N; KK = K;
        for(i=0; i < NN; i++) { Pi[i] = i; }
	if (KK > 0) derange_k(NN-1, KK);
	else derange(NN-1);
	printf("</TABLE>");
        printf("<p><b>Derangements = %d </b></p>", count);
        
}


void swap(int i, int j) {
    int temp;
    temp = Pi[i]; Pi[i] = Pi[j]; Pi[j] = temp;
}


void derange_k(int n, int k)
{
  int i;
  calls++;
  if (n < 0) PrintIt();
  else {
     // Add n to existing cycles
     if ((n+1) >= 2*k) {
        for (i=NN-1; i >= n+1; i--) {
           swap( i, n );  derange_k( n-1, k );  swap( i, n );
        } 
     } 
     // Make 2-cycles containing n
     if ((n > 0) && (k > 0) && ((n+1) >= 2*k)) {
        swap( n, n-1 );
        derange_k( n-2, k-1 );
        for (i = n+1; i<NN; i++) {
           swap( Pi[i], n );  swap( i, n-1 );  
           derange_k( n-2, k-1 );  
           swap( i, n-1 );  swap( Pi[i], n );  
        }
        swap( n, n-1 );
     }
   }
} // end of derange_k


void derange(int n)
{
  int i;
  calls++;
  if (n < 0) PrintIt();
  else {
     // Add n to existing cycles
        for (i=NN-1; i >= n+1; i--) {
           swap( i, n );  derange( n-1 );  swap( i, n );
        } 
     // Make 2-cycles containing n
     if (n > 0) {
        swap( n, n-1 );
        derange( n-2 );
        for (i = n+1; i<NN; i++) {
           swap( Pi[i], n );  swap( i, n-1 );  
           derange( n-2 );  
           swap( i, n-1 );  swap( Pi[i], n );  
        }
        swap( n, n-1 );
     }
   }
} // end of derange

#include "commonio.cc"
	
    
