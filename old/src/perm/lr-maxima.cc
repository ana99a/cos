//
// This program generates permutations with a given left-right
// maxima.  The algorithm is essentially an implementation of
// the Stirling-1 recurrence relation.
//
// 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "ProcessInput.c"
#include "commonio.h"


void swap(int, int);
void generate(int, int);

int NN, KK;	
int main(int argc, char **argv) {
    int i;

    ProcessInput(argc, argv); NN = N; KK = K;
    // No initialization of permutation array necessary
    // Other than first element?

    Pi[0] = NN-1; // Working backwards....

    count = 0;
    generate(1,1);
    printf("</TABLE>");
    printf("<p><b>Permutations :%d</b></p>",count);
}

void swap(int i, int j) {
    int temp;
    temp  = Pi[i]; Pi[i] = Pi[j]; Pi[j] = temp;
}

void generate(int n, int k) {
    int i, start;
    if (n >=NN) {
	    PrintIt(); 
    }
    else {
	Pi[n] =  (NN-1) - n;
	if ((NN-n) >= (KK-k)) {

	    if((NN-1 != n) || (k >= KK)) {   // Pruning tree
		generate(n+1,k); // Do the bit on end
	    
	    
		for (i=n; i>=2; i--) { // Loop backwards
		    swap(i, i-1);
		    generate(n+1, k);  // Don't increase # lr-maximae in the perm
		}
	    
		start = 1;
		//Now we increase size of k, the # lr-maximae
		if (k < KK) {
		    swap(0,1);
		    generate(n+1, k+1);
		    start = 0;
		}

	    } else {  // Move 0 to end, and process...
		start = 0;
		for(i=n; i>=1; i--) swap(i, i-1);
		generate(n+1, k+1);
	    }
	//Now restore the permutation to its former glory
	for(i=start; i<n; i++) swap(i, i+1);
	}
    }
}


void process_cmdline(int argc, char **argv)
{
char argument[MAX];
char *numptr;
int temp;
int i;
int n_ok=0; // Assume the command line is not OK for now
int k_ok=0;
// Process the command arguments

for(i=1; i<argc; i++) { //Process arguments one at a time
    strcpy(argument, argv[i]); // get the ith argument
    if((strlen(argument) >2) && (argument[0] == '-')) { // The argument is a switch
	numptr = argument; // Point to argument... we need to do this to obtain
	numptr +=2; // The actual number following a switch
	temp = atoi(numptr); // convert to a numeric argument
	switch(toupper(argument[1])) 
	    {
	    case 'N' : NN = temp;
		       n_ok  = 1;
		       break;
	    case 'K' : KK = temp;
		       k_ok = 1;
		       break;
	    case 'O' : out_format = temp;
		       break;
	    }
    }
}


}

#include "commonio.cc"
