#include <stdio.h>
#include "gd1.2/gd.h"
#include "stamps.h"
#define LIMIT_ERROR -1

int num = 0;

void gdShort(struct stamp *first, int N)
	{
	gdImagePtr im;
	int black, white, red, notQuiteBlack;
	FILE *out;
	char outName[80];

	int i;
	int width, height;

	num++;
	sprintf(outName, "/theory/www/per/tmpImages/test_%d_%d.gif",
PID, num);

	width = 30 + (N - 1) * 20;
	height = 80 + N * 5;
	im = gdImageCreate(width, height);
	/* allocate background color first */
	white = gdImageColorAllocate(im, 255, 255, 255);
        black = gdImageColorAllocate(im, 0, 0, 0);
	red = gdImageColorAllocate(im, 255, 0, 0);
	notQuiteBlack = gdImageColorAllocate(im, 0, 0, 1);

	/* draw the stamp folding */
	for (i = 15; i<= width - 15; i += 20)
		{
		gdImageLine(im, i, 20 + N * 2.5, i, 60 + N * 2.5, black);
		}
	/* specifically, draw the perforations */

	/* Write to disk and destroy image */
	out = fopen(outName, "wb");
	if (!out)
		{
		printf("The file didn't open.<BR></TR></TABLE>");
		exit(1);
		}
	gdImageGif(im, out);
	fclose(out);
	gdImageDestroy(im);

	printf("<TD>");
	printf("<IMG
SRC=../per/perm/displaygif.pl.cgi?pid=%d&num=%d>",
PID, num);
	printf("</TD>\n");

	return;
	}



