/* Program to enumerate all irreducible and/or primitive polynomials 
   over GF(4) 
   Gilbert Lee
*/

#include <stdio.h>
#include <stdlib.h>

typedef unsigned long long ULL;
/* Change to size of unsigned long long */
#define MAX 64
#define MAX_ITERATIONS 10000

ULL ONE = (ULL)1;
ULL TWO = (ULL)2;
ULL iter_limit = 0;

typedef struct{
  ULL top;
  ULL bot;
} Poly_GF4;
/* Each polynomial is expressed in two words.  00 = 1, 01 = 1, 10 = t, 11 = t+1 */

/* A list of primitive polynomials 
   poly_table[i] contains what x^i is equivalent to.
*/

Poly_GF4 poly_table[MAX+1] = {
/*  0 */ {(ULL)0ULL, (ULL)0ULL},
/*  1 */ {(ULL)1ULL, (ULL)0ULL},
/*  2 */ {(ULL)1ULL, (ULL)3ULL},
/*  3 */ {(ULL)3ULL, (ULL)5ULL},
/*  4 */ {(ULL)7ULL, (ULL)8ULL},
/*  5 */ {(ULL)13ULL, (ULL)21ULL},
/*  6 */ {(ULL)1ULL, (ULL)7ULL},
/*  7 */ {(ULL)53ULL, (ULL)86ULL},
/*  8 */ {(ULL)97ULL, (ULL)252ULL},
/*  9 */ {(ULL)41ULL, (ULL)155ULL},
/* 10 */ {(ULL)79ULL, (ULL)404ULL},
/* 11 */ {(ULL)167ULL, (ULL)758ULL},
/* 12 */ {(ULL)1ULL, (ULL)79ULL},
/* 13 */ {(ULL)649ULL, (ULL)2194ULL},
/* 14 */ {(ULL)1299ULL, (ULL)5159ULL},
/* 15 */ {(ULL)43ULL, (ULL)1966ULL},
/* 16 */ {(ULL)1ULL, (ULL)11ULL},
/* 17 */ {(ULL)523ULL, (ULL)10521ULL},
/* 18 */ {(ULL)1379ULL, (ULL)26770ULL},
/* 19 */ {(ULL)198279ULL, (ULL)316561ULL},
/* 20 */ {(ULL)408609ULL, (ULL)1000025ULL},
/* 21 */ {(ULL)866823ULL, (ULL)1325922ULL},
/* 22 */ {(ULL)314081ULL, (ULL)2087445ULL},
/* 23 */ {(ULL)679727ULL, (ULL)2954082ULL},
/* 24 */ {(ULL)69753ULL, (ULL)1146235ULL},
/* 25 */ {(ULL)13266581ULL, (ULL)18125753ULL},
/* 26 */ {(ULL)359383ULL, (ULL)6759284ULL},
/* 27 */ {(ULL)52080703ULL, (ULL)129429176ULL},
/* 28 */ {(ULL)1ULL, (ULL)16516ULL},
/* 29 */ {(ULL)234512349ULL, (ULL)438810515ULL},
/* 30 */ {(ULL)465571539ULL, (ULL)795799278ULL},
/* 31 */ {(ULL)846806239ULL, (ULL)1185262580ULL},
/* 32 */ {(ULL)1632550249ULL, (ULL)4256918206ULL}
#if (MAX > 32)
,
/* 33 */ {(ULL)3321846741ULL, (ULL)4940296265ULL},
/* 34 */ {(ULL)6707052655ULL, (ULL)16430663302ULL},
/* 35 */ {(ULL)2614786921ULL, (ULL)14151316494ULL},
/* 36 */ {(ULL)1501885783ULL, (ULL)15595575074ULL},
/* 37 */ {(ULL)10793672579ULL, (ULL)34889484807ULL},
/* 38 */ {(ULL)21926001731ULL, (ULL)88663510867ULL},
/* 39 */ {(ULL)210872051723ULL, (ULL)309625635897ULL},
/* 40 */ {(ULL)21285117ULL, (ULL)7370011650ULL},
/* 41 */ {(ULL)830718098427ULL, (ULL)2161938897166ULL},
/* 42 */ {(ULL)347946500483ULL, (ULL)1169890096278ULL},
/* 43 */ {(ULL)3385366402441ULL, (ULL)8255117775764ULL},
/* 44 */ {(ULL)88799841899ULL, (ULL)2011688021110ULL},
/* 45 */ {(ULL)14593314216525ULL, (ULL)24168557018481ULL},
/* 46 */ {(ULL)1642293838367ULL, (ULL)14266689509455ULL},
/* 47 */ {(ULL)55130667757517ULL, (ULL)78951024793390ULL},
/* 48 */ {(ULL)22072872957351ULL, (ULL)92860701335807ULL},
/* 49 */ {(ULL)2392622704237ULL, (ULL)40733933189842ULL},
/* 50 */ {(ULL)19996129314071ULL, (ULL)156405551508016ULL},
/* 51 */ {(ULL)172263016296039ULL, (ULL)848653852685204ULL},
/* 52 */ {(ULL)1712390431256129ULL, (ULL)2754485859980332ULL},
/* 53 */ {(ULL)674753133110323ULL, (ULL)3806517156287502ULL},
/* 54 */ {(ULL)7640240529687611ULL, (ULL)12536583063847610ULL},
/* 55 */ {(ULL)13851508086332803ULL, (ULL)34232383058160638ULL},
/* 56 */ {(ULL)18756063164753ULL, (ULL)1656658666037115ULL},
/* 57 */ {(ULL)54444906484098359ULL, (ULL)141656984603640671ULL},
/* 58 */ {(ULL)122182521732613139ULL, (ULL)200972344765876531ULL},
/* 59 */ {(ULL)229207683966486553ULL, (ULL)532183231166632916ULL},
/* 60 */ {(ULL)4562265008713767ULL, (ULL)105791251697411146ULL},
/* 61 */ {(ULL)53775334883496721ULL, (ULL)463974985935930722ULL},
/* 62 */ {(ULL)1875866505772887223ULL, (ULL)3716727209282143739ULL},
/* 63 */ {(ULL)3901015008527977193ULL, (ULL)7968093027693979277ULL},
/* 64 */ {(ULL)7383295237147553367ULL, (ULL)9675520339785773085ULL}
#endif
};

/* Global Variables */
int N,D;
int outformat, type;
ULL maxNumber;
ULL ir_count = 0, pr_count = 0;
int printed = 0, doneFlag = 0;
Poly_GF4 fourN_1;
int a[MAX+1] = {0};
ULL tracecount[4][4];

/* Printing routines */
void PrintString(Poly_GF4 a){
  int i;
  printf("1");
  for(i = N-1; i >= 0; i--)
    if(a.top&(ONE<<i)){
      printf(" %c", (a.bot & (ONE << i)) ? 'b' : 'a');
    } else {
      printf(" %c", (a.bot & (ONE << i)) ? '1' : '0');
    }
  printf("\n");
}

void PrintCoeff(Poly_GF4 a){
  int i;
  printf("%d<sub>1</sub>", N);
  for(i = N-1; i >= 0; i--)
    if((a.top|a.bot)&(ONE<<i)) printf(" %d<sub>%c</sub>", i,(a.top&(ONE<<i))? (a.bot&(ONE<<i))?'b':'a':'1');
  printf("\n");
}

void PrintPoly(Poly_GF4 a){
  int i;
  printf("x<sup>%d</sup> + ", N);
  for(i = N-1; i > 0; i--){
    if(a.top & (ONE << i)){
      printf("%c", a.bot & (ONE << i) ? 'b' : 'a');
      if(i == 1) printf("x + ");
      else printf("x<sup>%d</sup> + ", i);
    } else if(a.bot & (ONE << i)){
      if(i == 1) printf("x + ");
      else printf("x<sup>%d</sup> + ", i);      
    }
  }
  if(a.top & (ONE << i)){
    printf("%c", a.bot & (ONE << i) ? 'b' : 'a');
  } else {
    printf("%c", a.bot & (ONE << i) ? '1' : '0');
  }
  printf("\n");
}

void PrintBit(Poly_GF4 a){
  int i;
  for(i = N-1; i >= 0; i--)
    printf("%d", a.top&(ONE<<i) ? 1 : 0);
  printf("\n");
  for(i = N-1; i >= 0; i--)
    printf("%d", a.bot&(ONE<<i) ? 1 : 0);
  printf("\n");
}

void PrintNum(Poly_GF4 a){
  int i;
  for(i = N-1; i >= 0; i--){
    if(a.top & (ONE << i)){
      if(a.bot & (ONE << i)){
	printf("3");
      } else {
	printf("2");
      }      
    } else {
      if(a.bot & (ONE << i)){
	printf("1");
      } else {
	printf("0");
      }
    }
  }
}

/* Counting bits in O(number of bits) */
int bitcount(ULL x){
  int c = 0;
  while(x){
    c++;
    x &= x-1;
  }
  return c;
}

/* Density - (By our encoding, we count number of bits in either word) */
int density(Poly_GF4 a){
  return bitcount(a.top|a.bot);
}

/* Returns a + b */
Poly_GF4 add(Poly_GF4 a, Poly_GF4 b){
  a.top ^= b.top;
  a.bot ^= b.bot;
  return a;
}

/* Returns -1 if a < b, 0 if a == b, +1 if a > b (a,b base 4 numbers)*/
int cmp(Poly_GF4 a, Poly_GF4 b){
  int i, x, y;
  for(i = N-1; i >= 0; i--){
    x = (a.top & (ONE<<i)) ? ((a.bot & (ONE<<i)) ? 3 : 2) : (a.bot & (ONE<<i)) ? 1 : 0;
    y = (b.top & (ONE<<i)) ? ((b.bot & (ONE<<i)) ? 3 : 2) : (b.bot & (ONE<<i)) ? 1 : 0;
    if(x < y) return -1;
    if(x > y) return 1;
  }
  return 0;
}

/* Returns a - b, (assuming a > b and a,b are base 4 numbers */
Poly_GF4 subtract(Poly_GF4 a, Poly_GF4 b){
  int i, borrow = 0, x, y;
  Poly_GF4 r = {0,0};
  for(i = 0; i < N; i++){
    x = ((a.top & (ONE<<i)) ? 2 : 0) + ((a.bot & (ONE<<i)) ? 1 : 0) - borrow;
    y = ((b.top & (ONE<<i)) ? 2 : 0) + ((b.bot & (ONE<<i)) ? 1 : 0);
    if(x < y){
      borrow = 1;
      x += 4;
    } else {
      borrow = 0;
    }
    switch(x-y){
    case 1: r.bot |= (ONE<<i); break;
    case 2: r.top |= (ONE<<i); break;
    case 3: r.bot |= (ONE<<i); r.top |= (ONE<<i); break;
    }
  }
  return r;
}

/* Returns a % b (a and b treated as base 4 numbers) */
Poly_GF4 mod(Poly_GF4 a, Poly_GF4 b){
  Poly_GF4 r = {0,0};
  int i;

  for(i = N-1; i >= 0; i--){
    r.top <<= 1;
    r.bot <<= 1;
    if(a.top & (ONE<<i)) r.top++;
    if(a.bot & (ONE<<i)) r.bot++;
    while(cmp(b,r) <= 0){
      r = subtract(r, b);
    }
  }
  return r;
}

/* Returns the gcd of a and b (a and b treated as ternary numbers */
Poly_GF4 gcd(Poly_GF4 a, Poly_GF4 b){
  if(!b.top && !b.bot) return a;
  return gcd(b, mod(a, b));
}

/* Returns (a * b) mod p (where p = poly_table[N]) */
Poly_GF4 multmod(Poly_GF4 a, Poly_GF4 b){
  Poly_GF4 s, t = a, result = {0,0};
  ULL top_bit = ONE << (N-1);
  int i;
  if((!a.top && !a.bot) || (!b.top && !b.bot)) return result;
  for(i = 0; i < N; i++){
    /* Add b[i] * a */
    if(b.top & ONE){
      if(b.bot & ONE){
	s.top = t.bot;
	s.bot = t.top ^ t.bot;
      } else {
	s.top = t.top ^ t.bot;
	s.bot = t.top;
      }
      result = add(result, s);
    } else if(b.bot & ONE){
      result = add(result, t);
    }
    b.top >>= 1;
    b.bot >>= 1;

    /* shift t, modulo p */    
    if(t.top & top_bit){
      t.top = (t.top & ~top_bit) << 1;
      if(t.bot & top_bit){
	t.bot = (t.bot & ~top_bit) << 1;
	s.top = poly_table[N].bot;
	s.bot = poly_table[N].top ^ poly_table[N].bot;
      } else {
	t.bot <<= 1;
	s.top = poly_table[N].top ^ poly_table[N].bot;
	s.bot = poly_table[N].top;
      }
      t = add(t,s);
    } else {
      t.top <<= 1;
      if(t.bot & top_bit){
	t.bot = (t.bot & ~top_bit) << 1;
	t = add(t,poly_table[N]);
      } else {
	t.bot <<= 1;
      }
    } 
  }
  return result;
}

/* Returns (a^power) mod p (where p = poly_table[N]) */
Poly_GF4 powmod(Poly_GF4 a, Poly_GF4 power){
  Poly_GF4 t = a, result = {0,ONE};
  while(power.top || power.bot){
    if(power.top & ONE){
      result = multmod(result, t);
      result = multmod(result, t);
    }
    if(power.bot & ONE) result = multmod(result, t);
    t = multmod(t, t);
    t = multmod(t, t);
    power.top >>= 1;
    power.bot >>= 1;
  }
  return result;
}

/* Calculates the minimum polynomial, given a necklace */
Poly_GF4 minpoly(Poly_GF4 necklace){
  Poly_GF4 root = {0,TWO}, result = {0,0};
  Poly_GF4 f[MAX]; /* f[i] contains the polynomials listing the
		      coefficient of x^i in terms of the primitive root */
  int i, j;

  f[0].top = 0ULL;
  f[0].bot = ONE;
  for(i = 1; i < N; i++)
    f[i].top = f[i].bot = 0ULL;

  root = powmod(root, necklace); 
  for(i = 0; i < N; i++){
    if(i){
      root = multmod(root, root);
      root = multmod(root, root);
    }
    for(j = N-1; j >= 1; j--)
      f[j] = add(f[j-1], multmod(f[j], root));
    f[0] = multmod(f[0], root);
  }
  
  for(i = N-1; i >= 0; i--){
    if(f[i].top == ONE){
      result.top |= ONE<<i;
      if(f[i].bot == ONE) result.bot |= ONE<<i;
      else if(f[i].bot) {
	/*
	  fprintf(stderr, "TROUBLE\n");
	  fprintf(stderr, "%d:[%llu,%llu]\n", i, f[i].top, f[i].bot);
	  exit(0);	
	*/
      }
    } else if(f[i].top){
      /*
	fprintf(stderr, "TROUBLE\n");
	fprintf(stderr, "%d:[%llu,%llu]\n", i, f[i].top, f[i].bot);
	exit(0);	
      */
    } else if(f[i].bot == ONE){
      result.bot |= ONE<<i;
    } else if(f[i].bot){
      /*
	fprintf(stderr, "TROUBLE\n");
	fprintf(stderr, "%d:[%llu,%llu]\n", i, f[i].top, f[i].bot);
	exit(0);
      */
    }
  }
  return result;
}

void OutputPoly(Poly_GF4 poly){
  switch(outformat){
  case 0: PrintBit(poly); break;
  case 1: PrintCoeff(poly); break;
  case 2: PrintPoly(poly); break;
  }
  printed++;
}

void PrintIt(){
  int i;

  Poly_GF4 necklace = {0,0};
  Poly_GF4 gcdResult;
  Poly_GF4 mpoly;

  if(++iter_limit > MAX_ITERATIONS){
    printf("</TABLE>\n<BR>");
    if (type == 0) {
      printf( "The first %llu irreducible polynomials<BR>\n", ir_count);
      printf("<FONT COLOR=0000ff>");
    }
    printf( "The first %llu primitive polynomials\n<br><br>", pr_count );
    printf("<br><b><blink><font color=ff0000>Iteration limit exceeded !!</B><br></blink></font>\n");
    exit(0);     
  }      

  for(i = 1; i <= N; i++){
    necklace.top <<= 1;
    necklace.bot <<= 1;
    if(a[i] % 2) necklace.bot++;
    if(a[i] > 1) necklace.top++;
  }
  
  mpoly = minpoly(necklace);
  if(density(mpoly)+1 > D) return;
  
  ir_count++;

  /*
  trace = ((mpoly.top & (ONE<<(N-1))) ? 2 : 0) + ((mpoly.bot & (ONE<<(N-1))) ? 1 : 0);
  subtrace = ((mpoly.top & (ONE<<(N-2))) ? 2 : 0) + ((mpoly.bot & (ONE<<(N-2))) ? 1 : 0);
  tracecount[trace][subtrace]++;
  */

  gcdResult = gcd(fourN_1, necklace);
  if(gcdResult.top == 0 && gcdResult.bot == ONE){
    ++pr_count;
    ++printed;
    printf("<TR>");
    if(outformat & 1){
      printf("<TD>");
      if(type == 0) printf("<FONT COLOR=0000ff>");
      PrintString(mpoly);
      if(type == 0) printf("</FONT>");
    }
    if(outformat & 2){
      printf("<TD>");
      if(type == 0) printf("<FONT COLOR=0000ff>");
      PrintCoeff(mpoly);
      if(type == 0) printf("</FONT>");
    } 
    if(outformat & 4){
      printf("<TD>");
      if(type == 0) printf("<FONT COLOR=0000ff>");
      PrintPoly(mpoly);
      if(type == 0) printf("</FONT>");
    }    
  } else if(type == 0){
    ++printed;
    printf("<TR>");
    if(outformat & 1){
      printf("<TD>");
      PrintString(mpoly);
    } 
    if(outformat & 2){
      printf("<TD>"); 
      PrintCoeff(mpoly);
    }
    if(outformat & 4){
      printf("<TD>");
      PrintPoly(mpoly);
    }    
  }
  if(maxNumber && printed >= maxNumber){
    printf("</TABLE>"); 
    exit(1);
  }
}

void Gen(int t, int p){
  int j;
  if(doneFlag) return;
  if(t > N){
    if(p == N) PrintIt();
  } else {
    a[t] = a[t-p]; 
    Gen(t+1,p);
    for(j=a[t-p]+1; j<=3; j++) {
      a[t] = j; 
      Gen(t+1,t);
    }
  }
}

void ProcessInput(int argc, char *argv[]){
  int i;
  outformat = 0;
  type = 0;
  maxNumber = 0;
  
  if (argc > 1) N = atoi(argv[1]);
  if (argc > 2) D = atoi(argv[2]); else D = N+1;
  if (argc > 3) outformat = atoi(argv[3]); else outformat = 1;
  if (argc > 4) type = atoi(argv[4]); 
  if (argc > 5) maxNumber = atoi(argv[5]);
  if (argc > 6 || N < 2 || N > MAX){
    fprintf(stderr,"Usage: gf4 n d format type number\n");
    fprintf(stderr,"         All params except 'n' are optional\n");
    fprintf(stderr,"         n = degree (2-%d)\n", MAX);
    fprintf(stderr,"         d = density (2-%d)\n", MAX+1);
    fprintf(stderr,"         format = 0,1,2,3 = string,ceoffs,poly,none\n");
    fprintf(stderr,"         type = 0/1 = all/primitive only\n");
    fprintf(stderr,"         num = max to find (0 for all)\n");
    exit( 1 );
  }
  /*
  printf( "degree      = %d\n", N );
  printf( "format      = %d\n", outformat );
  printf( "type        = %s\n", type ? "primitive" : "all" );
  printf( "num         = %lld %s\n", maxNumber, maxNumber ? "" : "(none)" );
  */
  for(i = 0; i < N; i++)
    fourN_1.top |= (ONE<<i);
  fourN_1.bot = fourN_1.top;
}

int main(int argc, char **argv){
  ProcessInput(argc, argv);

  printf("N = %d.  Maximum density = %d<BR>", N, D);
  printf("a = Root of y<sup>2</sup> + y + 1, b = a + 1<BR>");
  printf("<P><TABLE BORDER=1 CELLSPACING=2 CELLPADDING=2><TR>\n");
  if (outformat & 1) {
    printf("<TH COLSPAN=1><FONT SIZE=+1>String</FONT><BR></TH>\n");
  }
  if (outformat & 2) {
    printf("<TH COLSPAN=1><FONT SIZE=+1>Coefficients</FONT><BR></TH>\n");
  }
  if (outformat & 4) {
    printf("<TH COLSPAN=1><FONT SIZE=+1>Polynomials</FONT><BR></TH>\n");
  }
    
  Gen(1,1);
  printf("</TABLE>\n<BR>");
  if(type == 0){
    printf("The number of irreducible polynomials = %llu<BR>\n", ir_count);
    printf("<FONT COLOR = 0000ff>");
  } 
  printf("The number of primitive polynomials   = %llu\n", pr_count);
  return 0;
}
