/*--------------------------------------------------------------------------
This program generates all non-isomorphic chord diagrams.
Experimentally, the program runs in constant amortized time.

Developed by: Joe Sawada 1999.
--------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>

#define MAX 50

typedef struct cell {
	int prev, next;
} cell;

cell avail[MAX];		
int a[MAX];
int pos[MAX][MAX];
int num_chords, N, head;
int total = 0;
int beads = 0; /* whether or not to print colored beads representation */
int string = 0; /* whether or not to print string representation */

/*--------------------------------------------------------------------------*/
/* List Functions */
/*--------------------------------------------------------------------------*/

void Init_list() {

	int i;
 
	for (i=1; i<N; i++) {
		avail[i].next = i+1;
		avail[i].prev = i-1;
	}
	head = 1;
}

void Remove( int i) {

	int p,n;
 
        if (avail[i].prev == 0) {
		head = avail[i].next;
		avail[head].prev = 0;
	}
        else  {
		p = avail[i].prev;		
		n = avail[i].next;		
		avail[p].next = n;
		avail[n].prev = p;
	}
	avail[i].next = i;
}
 
void Add( int i) {
 
	int p,n;

        if (avail[i].prev == 0) {
                avail[head].prev = i;
		avail[i].next = head;
                head = i;
        }
        else {
		p = avail[i].prev;
		n = avail[p].next;
                avail[i].next = n;
		avail[n].prev = i;
                avail[p].next = i;
        }
}
/*--------------------------------------------------------------------------*/

void Print() {

	int i;
	printf("<tr><td>");
	if ( string == 1 )
      for (i=0; i<N; i++) printf("%d ", a[i]);

    if ( beads == 1) {
       /* alternate representation */
       int b[N];
       int current_chord = 0;
       /* initialize b */
       for  (i=0; i<N; i++) b[i] = -1;
       /* store alternate representation in b */
       for (i=0; i<N; i++) {
          int length = a[i];
          if (b[i] == -1)  {
             current_chord++;
             b[i] = current_chord;
             if (i+length >= N) printf("Overwriting array.");
             else b[i+length] = current_chord;
          } /* if */
       } /* for */
       printf("</td><td>");
       for (i=0; i<N; i++) {
           /*printf("%d ", b[i]);*/
           if (b[i] == 1) {
             printf ("<Image src = ico/redball.gif>");
           }
           else if (b[i] == 2) {
              printf ("<Image src = ico/blueball.gif>");
           }
           else if (b[i] == 3) {
              printf ("<Image src = ico/greenbal.gif>");
            }
           else if (b[i] == 4) {
               printf ("<Image src = ico/newhibal.gif>");
            }
           else if (b[i]== 5) {
               printf ("<Image src = ico/orangeba.gif>");
            }
           else if (b[i] == 6) {
               printf ("<Image src = ico/purpleba.gif>");
            }
           else if (b[i] == 7) {
               printf ("<Image src = ico/yellowba.gif>");
            }

       } /* for */
    } /* if beads == 1 */

	printf("</td></tr>\n");	   
	total++;	 
}
/*--------------------------------------------------------------------------*/
void GenRest(int s, int e, int v) {

	if (s == N) Print(); 
	else if (e < N) {
		if (e-s >= v && N-e+s >= v) {
			a[s] = e - s; a[e] = N - a[s];
			Remove(s); Remove(e);
                        GenRest(head,avail[head].next,v);
                        Add(e); Add(s);
		}
		if (N-avail[e].next+s >= v) GenRest(s,avail[e].next,v);
	}
}

/*--------------------------------------------------------------------------*/

void Gen2(int s, int t, int p, int T, int P, int len, int num_sec) {

	int e, next, min;

	min  = pos[len][t-p];
	if (len == num_chords && s > num_chords) {
		if (T == num_chords) Print();
	} 
	else if (t == 1 && s > P) {
		if (len < num_chords) Gen2(head,1,1,T,P,len+1,0);
	}  
	else if (s > (num_sec+1)*P ) {
		pos[len][t] = P;
		if (min == P)  Gen2(s,t+1,p,T,P,len,num_sec+1);
		else  Gen2(s,t+1,t,T,P,len,num_sec+1);
	}
	else if (s == N) {
		if (T == num_chords) Print();
                        else if (min != P)  
				GenRest(head,avail[head].next,len+1);
                        else if (t%p == 0 && len < num_chords) 
				Gen2(head,1,1,T,N*p/t,len+1,0);
	}
	else {
		e = (s + len) % N;
		if (s % P >= min && avail[e].next != e) {
			next = avail[s].next;
			if (next == e) next = avail[e].next;
			a[s] = (N+ e -s) % N; 
			a[e] = N - a[s];
			Remove(s); Remove(e);
			pos[len][t] = s % P;
			if (s % P == min)  
				Gen2(next,t+1,p,T+1,P,len,num_sec);
			else 
				Gen2(next,t+1,t,T+1,P,len,num_sec);
			Add(e); Add(s);
		}
		Gen2(avail[s].next,t,p,T,P,len,num_sec);
	}
}
/*--------------------------------------------------------------------------*/

void Gen(int s, int t, int p, int v, int last) {

	int next,e,min,p2;

	min = last + pos[v][t-p];
	if (min == N && t%p == 0) {
 		if (t == num_chords) Print();
                else Gen2(head,1,1,t,N*p/t,v+1,0);
	}
	else if (min < N && s < N) {
		e = (s + v) % N;
		if (s >= min && avail[e].next != e) {

			next = avail[s].next;
			if (next == e) next = avail[e].next;
		 	a[s] = v; a[e] = N - v;
			Remove(s); Remove(e);
			pos[v][t] = s-last;
	
			p2 = p;
			if (s != min) p2 = t; 
			Gen(next,t+1,p2,v,s);
			if (s + pos[v][t+1-p2] < N)
				GenRest(head, avail[head].next,v+1);
			Add(e); Add(s);
		} 
		Gen(avail[s].next,t,p,v,last);
	}
}

/*--------------------------------------------------------------------------*/

int main(int argc, char *argv[]) {
 
        int i;
	num_chords = atoi(argv[1]);
        N = 2*num_chords;  
	printf("<table border=1><tr>");
	if ((atoi(argv[2]) == 1) || (atoi(argv[2]) == 3)) {
		printf("<th>Standard<br />Representation</th>");
        string = 1;
    } /* if */
	if ((atoi(argv[2]) == 2) || (atoi(argv[2]) == 3))  {
		printf("<th>Colored Beads</th>");
        beads = 1;
    } /* if */
	printf("</tr>");
	Init_list();

	total = 0; 
	for (i=1; i<num_chords; i++) pos[i][0] = 0;	
        for (i=1; i<num_chords; i++) {
		a[0] = i;
		a[i] = N-i;
                Remove(i);
                Gen(head,1,1,i,0);   
		GenRest(head, avail[head].next, i+1);
                Add(i);
        }
	for(i=0; i<N; i++) a[i] = num_chords;

	Print();
	printf ("</table>");
        printf("<br /><br />There are %d objects with %d chords.", total, num_chords); 
	return 0;
}

