/* hooks.h - Linear Extension Hooks and Client Procs */

/* Copyright 1991, Kenny Wong and Frank Ruskey. All Rights Reserved.
 *
 * This program is free software. You may use this software, and copy,
 * redistribute, and/or modify it under the terms of version 2 of the 
 * GNU General Public License. This and all modified works may not be
 * sold for profit. Please do not remove this notice or the original 
 * copyright notices.
 *
 * This software is provided "as is" and without any express or implied
 * warranties, including, without limitation, any implied warranties
 * of merchantability or fitness for a particular purpose. You bear all
 * risk as to the quality and performance of this software.
 *
 * You should have received a copy of the GNU General Public License
 * with this software (see the file COPYING); if not, write to the
 * Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Please report bugs, fixes, applications, modifications, and requests to:
 *
 * Kenny Wong or Frank Ruskey
 * Department of Computer Science, University of Victoria
 * PO Box 3055, Victoria, BC, Canada, V8W 3P6
 * Internet: kenw@csr.UVic.CA or fruskey@csr.UVic.CA
 *
 * All correspondence should include your name, address, phone number,
 * and preferably an electronic mail address.
 */

/*
 * Software Engineering Stuff
 * 
 * Given a linear extension, we might want to print it, 
 * compute its parity, or whatever. For flexibility, 
 * these "client actions" or "hooks" have been pulled
 * out of the generator modules and placed here. Also,
 * these hooks should not access the private variables 
 * of the generator modules; a hook should not depend 
 * on how the linear extension was produced.
 * A hook may be used by more than one generator, but 
 * not simultaneously.
 * 
 * A hook consists of five macros (for efficiency) and
 * perhaps some global variables. The macros are called 
 * at specific times during execution (see comments).
 *
 */

/* Hook to be invoked when "generating" ... does nothing for now! */

GLOBAL int gCount;

/* called when the generator is initialized */
#define uGenInit()

/* called prior to processing a new poset ... preprocessing stuff */
#define uGenBegin()

/* called for each linear extension */
#define uGenProc()

/* called after processing a poset ... postprocessing stuff */
#define uGenEnd()

/* called when the generator is shut down */
#define uGenQuit()


/* Hook to be invoked when printing ... */

#define uPrintInit()

#define uPrintBegin()

#define uPrintProc()		do { \
	register int i;							\
	for (i = 1; i <= gN; i++) {					\
	    (void)fprintf( stdout, "%d ", gExt[i] );			\
	}								\
	(void)fputc( '\n', stdout );					\
} while (0)

#define uPrintEnd()

#define uPrintQuit()


/* Hook to be invoked when computing the parity difference ... */

GLOBAL int gVisit[kN+2];
GLOBAL int *gVisitP;

GLOBAL int gDiff; /* parity difference = |evens| - |odds| */

#define uParityDiffInit()	do { \
	register int i;							\
	for (i = 1; i <= kN; i++) {					\
	    gVisit[i] = 0;						\
	}								\
	gVisitP = gVisit + 1;						\
} while (0)

/* globals gN, gE, gExt, are now known */
#define uParityDiffBegin()	do { \
	gDiff = 0;							\
} while (0)

/* examine the cycle structure of the linear extension ... O(gN) */
#define uParityDiffProc()	do { \
	register int parity;						\
	register int i, j, *vp;						\
	parity = 0; /* 0 is even, 1 is odd */				\
	for (vp = gVisitP, i = 1; i <= gN; i++) {			\
	    if (*vp) {							\
		*vp = 0;						\
	    } else {							\
		for (j = i; gExt[j] != i; parity = !parity)		\
		    gVisit[j = gExt[j]] = 1;				\
	    }								\
	    vp++;							\
	}								\
	parity ? gDiff--: gDiff++;					\
} while (0)
	
#define uParityDiffEnd()	do { \
	(void)fprintf( stdout, "Parity Diff: %d\n", gDiff );		\
} while (0)

#define uParityDiffQuit()
