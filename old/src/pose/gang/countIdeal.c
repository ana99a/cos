#include <stdio.h>
#include <stdlib.h>

#define MAX_NODE 100

enum MARKTYPE{VISITED, UNVISITED};

typedef struct node{
  int in;
  int out;
  enum MARKTYPE mark;
}NODE;
  
NODE graph[100];
int ROOT;
int N;                                   // # of nodes
int matrix[MAX_NODE][MAX_NODE];          // adjacency matrix

void print()
{
  int i;
  //printf("in: ");
  for(i = 1; i <= N; i++)
    printf("%d: %d %d, ",i, graph[i].in, graph[i].out );
  printf("\n");
}

void dfs1(int n)
{
  int i, temp, sum, temp_in, temp_out, sum_in, sum_out;
  int leaf_node;
  leaf_node = 1;
  graph[n].mark = VISITED;
  //printf("dfs(%d)\n", n);
  for(i = 1; i <= N; i++){
    //printf("%d\t", i);
    if(matrix[n][i] && (graph[i].mark == UNVISITED)){
      graph[i].in = graph[n].in;
      graph[i].out = graph[n].out + graph[n].in;
      // print();
      dfs1(i);
      sum = graph[n].in + graph[n].out;
      // if(sum == 0) { printf("n is %d\n", n); exit(1);}
      graph[n].in = graph[i].in + (graph[n].in * graph[i].out) / sum;
      graph[n].out = (graph[n].out * graph[i].out) / sum;
      // print();
    }
    else if(matrix[i][n] && (graph[i].mark == UNVISITED)){
      graph[i].in = 1;
      graph[i].out = 1;
     // print();
      dfs1(i);
      graph[n].out = graph[n].out*(graph[i].out + 1);
    }
  } 		     
}

void InitEdges()
{
  int i, j, e1, e2;
  for(i = 1; i <= N; i++){
    graph[i].mark = UNVISITED;
    graph[i].in = 0;
    graph[i].out = 0;
  }
  for(i = 1; i <= N; i++)
    for(j = 1; j<= N; j++)
      matrix[i][j] = 0;
  for (i=1; i < N; i++){
    printf("\nnext edge:");
    scanf("%d",&e1); 
    scanf("%d",&e2);
    matrix[e1][e2] = 1;
  }
}

int main()
{
  int i, j;
  printf("Input num of nodes:");
  scanf("%d", &N);
  
  InitEdges();
  ROOT = 1;
  graph[ROOT].in = 1;
  graph[ROOT].out = 1;
  dfs1(ROOT);
  
  printf("number of ideals is %d\n",graph[ROOT].in+graph[ROOT].out);
}
