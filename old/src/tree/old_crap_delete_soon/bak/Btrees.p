program recursive ( input, output );

var
  n, d, m, m2, num : integer;
  a : array [1..100] of integer;

function min ( x, y : integer ) : integer;
begin
  if x < y then min := x else min := y;
end;

procedure PrintIt( p : integer );
var i : integer;
begin
  num := num + 1;  write( '[',num:3,'] ' );
  for i := 1 to p do write( a[i]:3 );
  writeln;
end;

procedure P ( s, d, p : integer );
var i : integer;
begin
  if (s = 1) and (d = 0) then PrintIt( p-1 ) else
  if (s > m) and (d >= 0) then begin
     for i := m2 to min(s-m2,m) do begin
        a[p] := i;  P( s-i, d+1, p+1 );
     end;
  end else
  if (s = m) then begin
     if not odd( m ) then begin
        a[p] := m2;  P( m2, d+1, p+1 );
     end;
     a[p] := m;  P(d+1, 0, p+1 );
  end else 
  if (s >= m2) and (d > 0) or (s >= 2) then begin
     a[p] := s;  P( d+1, 0, p+1 );
  end;   
end {of P};

begin
  (* write( 'Enter n, d, m: ' );  readln( n, d, m ); *)
  write( 'Enter n, m: ' );  readln( n, m ); 
  m2 := (m+1) div 2;
  num := 0;
  P( n, 0, 1 );
end.

