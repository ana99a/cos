/* program  : freetree.c            */
/* purpose  : generating free trees  */
/* input    : n -- number of nodes */
/*            m -- max degree */
/*            lb,ub -- lower and upper bound of diameter */
/* output   : listing of free trees in relex order*/ 
/* date     : Sept, 1995             */
/* author   : Modified from Ruskey & Li's program on rooted tree by */
/*           Gang LI        */
/* modified : Apr, 2013 by Michael Miller */
/* added    : one check to output_format for drawing*/
#include <stdio.h>
#include "ProcessInput.c"
#include "OutputTrees.c"

#define MaxSize   50  /* max size of the tree */
#define true      1
#define false     0

int
 par[MaxSize],                  /* parent position of i  */
 L[MaxSize],                    /* level of node i       */
 k,                             /* max number of children */
 chi[MaxSize],                  /* number of children of a node */
 nextp[MaxSize],                /* next good pos to add nodes */
 rChi[MaxSize],                 /* the right most child of node i */
 ub;                            /* upper bound           */


void updateL()
{ int i;
    L[1]=0;
    for(i=2;i<=N;i++) L[i]=L[par[i]]+1;
}

int good(p,h,t)   
int p,h,t;
{
if (p==2 && K<=2 && t==0) return true;
if (t == 1 ) {if (2*h>=K+1 && 2*h <= U+1) {
  if ((p-1)*2 >= N) return true;  else
  if (p - h-1 == 1) {if (par[p]> 2) return true;} else
  if ((p - h-1 >=2) && ((par[h+2]>2) || (par[h+3]>2))) return true;
}} else if (N-p >= h && 2*h>=K)
  { if (U==N-1 && N%2==0) return(2*h<=U+1); else return(2*h <= U);}
return false;
}   

void Gen( p, s, cL,h,l, n, f,g) 
int p,s,cL,h,l,n,f,g;
{
int hh,flag,entry,temp;
  if (p > n) 
   if (f == 0) {
    if (good(p-1,h,0)) Gen(p, 2, p-2,h,n,N,1,0); 
    if (good(p-1,h,1)) Gen(p, 2, p-3,h,n,N,1,1);
   } else {updateL(); PrintIt(N,par,L);}
  else {
    if (cL == 0) {if (p<ub+2) par[p] = p-1;else {
      Gen(p,p-1,1,h,l,n,f,g);
      return; }} else  
    if (par[p-cL] < s) par[p]=par[p-cL];       
    else {
      par[p] = cL + par[p-cL];
      if (g==1) 
        if (((l-1)*2 < n) 
           && (p-cL<=l) && (
               ((p-cL+1<l) &&  (par[p-cL+1]==2)  
               && (p-cL+2<=l) && (par[p-cL+2]==2))     /*case 1*/
           || ((p-cL+1==l) && (par[p-cL+1]==2))        /*case 2*/
           || (p-cL+1>l))) {                           /*case 3*/
              s= par[p]; cL= p-s;
              par[p] = par[par[p]];
        } else if (par[p-cL]==2) par[p]=1;  
    }
    if (s!=0 || p<=ub+1) {
       chi[par[p]] = chi[par[p]] + 1;
       temp = rChi[par[p]]; rChi[par[p]] = p;
       if (chi[par[p]] <= ((par[p]==1)?k:k-1)) {
          if (chi[par[p]] < (par[p]==1?k:k-1)) nextp[p] = par[p];
          else nextp[p] = nextp[par[p]];
          Gen( p+1, s, cL,h,l,n,f,g ); 
       }
       chi[par[p]] = chi[par[p]] - 1;
       rChi[par[p]] = temp;
    }
    if (s==0 && 2*(p-2)<K) return;

    nextp[p] = nextp[par[p]];
    entry = nextp[p];
    flag= 0; hh=1;
    while ((((f==0) && (entry>=2)) ||
	   ((f==1) && (entry>=1))) && (flag==0)) {
       if (s==0) h = p-2;
       if (p<=l+h-g) hh = 0; 
       if ((f==0) || (hh==1)) {
        /*s = par[p]; par[p] = par[s];*/
        par[p] = entry;

        chi[entry] = chi[entry] + 1;
	temp = rChi[par[p]]; rChi[par[p]] = p;
	if (chi[entry] >= (entry==1?k:k-1)) nextp[p] = nextp[entry];
        if (f==0) Gen(p+1,temp,p-temp,h,0,N-h+1,f,g);
        else if (hh==1) Gen(p+1,temp,p-temp,h,l,n,f,g);
	chi[entry] = chi[entry] - 1;
	rChi[par[p]] = temp;
	entry = nextp[entry];
	nextp[p] = entry;
       } else flag= 1;
    }
    if (f == 0) {
     if (good(p-1,h,0)) Gen(p,2,p-2,h,p-1,N,1,0);
     if (good(p-1,h,1)) Gen(p,2,p-3,h,p-1,N,1,1);
    }
  }
}


main(argc, argv)
int argc;
char *argv[];
{
  int i = 0;

  /* first set all the parameters */
  ProcessInput(argc, argv);
  if (out_format & 1) outputP = 1;
  if (out_format & 2) outputL = 1;
  if (out_format & 8) outputPNG = 1;
  if (K > U) {printf("<p>ERROR: lb > ub !!<br>\n"); exit(1);}
  if (U>1) {
    if (U<N) ub = (U+1)/2; else {ub = N/2; U=N-1;} }
  else {printf("<I>ub</I> is too small, should be >= 2\n"); exit(0);}
  if (M>0) k=M; else k= -1;

  if (K<=1) printf("k is too small, should be >= 2\n"); else { 
    for(i=1;i<=N;i++) chi[i]=0;

    par[1] = 0; par[2] = 1; nextp[1]=0; nextp[2]=1;chi[1]=1;
    Gen( 3, 0, 0,ub,0,N-ub+1,0,0); 

    printf("</table><BR>The total number of trees is : %4d<BR>\n",num);
  }
  exit(0);
}
