/*
 * File modified Apr. 11 2013 by Michael Miller
 * added drawing for rooted free trees
 */


#ifndef OUTPUTTREES_C 
#define OUTPUTTREES_C


#include <unistd.h>
#include <stdio.h>
#include "gd1.2/gd.h"
#define LIMIT_ERROR -1
#include "ProcessInput.c"
#include "MakeTree.c"

int num=0;       /* counter for the number of objects generated */
int outputP=0,   /* output parent array */
    outputG=0,   /* output Gray code    */
    outputL=0,   /* output level sequence */
    outputB=0,   /* output balls   */
    outputR=0,   /* output reverse of parent array  */
    outGif =0,   /* output GIFs    */
    outputC=0,   /* output Compact Codes array */
  outputCC=0,    /* output ComCode-Tree array */
  outCCGif=0,    /* output ComCode-Tree tree */
  outputPNG=0,   /* output free trees */
  outComGif=0;   /* output Compact Codes GIFs */
int omit = 0;    /* omit the first tree ? */
int shift = 0;   /* number of initial elements to skip(no printout) */
char int_string[8]= ""; /*act as temp storage to conver the ints to chars for rooted free tree*/
char level_order[256]; /*store the level order representation as a string*/

int *seqPtr;  /* To store level sequence of tree - used in seq2tree */
int *Left;    /* To store Left Trees for Compact Codes */
int *Right;   /* To store Right Trees for Compact Codes */

/* Constants */

#define MAXKIDS 2
#define max(a, b) (a > b ? a : b)

/* Statuses (Statii?) used by the Knuth alg while numbering the
   nodes in the tree */

#define ST_FIRST_VISIT 1
#define ST_LEFT_VISIT 2
#define ST_RIGHT_VISIT 3

/* Tree definition */

typedef struct Node {
  int key;
  struct Node *father;
  struct Node *son[MAXKIDS];
  int height;
  int x, y;  /* These are the position coordinates on the output device */
  int status;

} Node;

typedef struct DimStruct {
  int x;
  int y;
} DimStruct;

/* Prototypes and other boring stuff */

Node *makeNode( void );
void inorderWalk(Node *root, DimStruct *dim);
void knuthAlg(Node *root);
Node *seq2tree(Node *root, int height);
Node *com2tree(Node *root, int height,  int currX);

void process_tree(Node *root, gdImagePtr im, int scale_factor, int white, int black, int red, int notQuiteBlack);

/*** The "Marsala" of the entire shabang ****/
void tree2gif(Node *root, DimStruct *dim, int scale_factor, int imgNum);


void PrintIt(n,x1,x2)
int n, *x1,*x2;
{ int i;

  DimStruct dim;
  Node *root;
  seqPtr = x1;
  dim.x = -1;
  dim.y = -1;
  num++;

  if (num <= LIMIT) {
    if (num == 1 && omit ) {omit=0; num--;} else {
      printf("<tr>");
      if (outputP) {
         printf("<td align=center>");
		 for(i=shift+1;i<=n+shift;i++)
		 {
				printf("%d ",x1[i]);
		 }
			 printf(" <br></td>");				  
      }
      if (outputR) {
                printf("<td align=center>");
                for(i=n+shift;i>=shift+1;i--)printf("%d ",x1[i]);
                printf("  <br></td>");
      }
	  /*The OR case is added so that even if level order is not requested the drawing can still occur*/
      if (outputL || outputPNG) {
			if(outputL) {	
                printf("<td align=center>");
				printf("<!-- outputRootedPNG = %d, outputL = %d -->", outputRootedPNG, outputL);
			}
				memset(level_order, 0, sizeof level_order);
				strcat(level_order, "\"");

            for(i=shift+1;i<=n+shift;i++)
            {
				if(outputL)
					printf("%d ",x2[i]);
                    /*Construct the string of the level order*/
					sprintf(int_string, " %d", x2[i]);
                	strcat(level_order, int_string);
            }
	
  				strcat(level_order, "\"");

			if(outputL)
				printf(" <br></td>");
			
      }
      if (outputB) {
                printf("<td align=center>");
                for(i=shift+1;i<=n+shift;i++) {
		 if (x1[i]==2) 
		     printf("<IMG SRC=ico/tiny.redball.gif>");
		 if (x1[i]==1) 
		     printf("<IMG SRC=ico/tiny.blackball.gif>");
		 if (x1[i]==0) 
		     printf("<IMG SRC=ico/tiny.whiteball.gif>");
                }
                printf(" <br></td>");
      }
      if (outputG) {
                printf("<td align=center>");
				printf("(%d ,%d )",x2[1]-shift,x2[2]-shift);
				/* printf("(%2d,%2d)",x2[1],x2[2]); */
                printf(" <br></td>");
      }
      if (outputC) {
	printf("<td align=center>");
	for(i=1;i<=n;i++) { printf("%d ", x1[i]); }
	printf(" <br></td>");
      }
      if (outputCC) {
	printf("<td align=center>");
	for(i=1;i <= n; i++) { printf("%d ", x1[i]); }
	printf(" <br></td>");
      }
      if (outCCGif) {
	root = (Node *) x2;
	knuthAlg(root);
	inorderWalk(root, &dim);
	tree2gif(root, &dim, 20, num);
	printf("<TD>");
	printf("<IMG SRC=per/tree/displaygif.pl.cgi?pid=%d&num=%d>",PID,num);
	printf("</TD>\n");
      }



/*Free Tree PNG requested, build the string for system call
 *Note the C system call was unable to find java without an explicit path given, this makes the code less portable
 *Note the fix was to find the location of the java executable and hard code the path in the call to java
 *Note the class files for the java stuff are located in ~/www/bin/tree
 */
	if (outputPNG){
        char fileOutName[80];
        char javaCall[1000] = "/opt/jdk1.6.0_43/bin/java -cp /theory/www/bin/tree  DrawingFreeTree ";
        int checkVal = -1;
		char currDir[100];		
	  	
		sprintf(fileOutName, "/theory/www/per/tree/treePNG/test_%d_%d.PNG ", PID, num);
        strcat(javaCall, fileOutName);
        strcat(javaCall, level_order);
    	checkVal = system(javaCall);

	/*Comment code to check on the command being passed, success checkVal == 0*/
		if(checkVal == 256){	
			char comment[400] = "<!-- 0 ";
			strcat(javaCall, "-->");
			strcat(comment,javaCall);
			printf(comment);
		}

		printf("<TD>");
		printf("<IMG SRC=per/tree/treePNG/test_%d_%d.PNG>",PID,num);
		printf("</TD>\n");
    }

      if(outGif) { /* Piccy's have been asked for */
                seqPtr++;
                /*** There would be some kind of "if" statement here */
#ifdef BINGRAYBIT_C
				seqPtr += shift;
#endif
                root = seq2tree(root, 0);
                knuthAlg(root);
                inorderWalk(root, &dim);
                tree2gif(root, &dim, 20, num);
                printf("<TD>");
                /* printf("<IMG SRC=\"../tmpImages/test_%d_%d.gif\"><BR>",PID, num); */
                /* printf("<IMG SRC=http://theory.cs.uvic.ca/per/tree/displaygif.pl.cgi?pid=%d&num=%d>",PID,num); CHANGED FR, June 28 */
                printf("<IMG SRC=per/tree/displaygif.pl.cgi?pid=%d&num=%d>",PID,num);
                printf("</TD>\n");
              }
      if(outComGif) { /* Piccy's have been asked for of compact codes*/
	Left = (int *) malloc(sizeof(int) * (2*n));
	Right= (int *) malloc(sizeof(int) * (2*n));

	MakeTree(x1, n, Left, Right);
	/*** There would be some kind of "if" statement here */
       	root = com2tree(root, 0, (2*n - 1));
	knuthAlg(root);
	inorderWalk(root, &dim);
	tree2gif(root, &dim, 20, num);
	printf("<TD>");
	printf("<IMG SRC=per/tree/displaygif.pl.cgi?pid=%d&num=%d>",PID,num);
	printf("</TD>\n");
      }



      if (out_format) printf("</tr>\n");
    }
  }
  else
  {
    exit(LIMIT_ERROR);
  }
}

/* Phunctions for tree manipulations */

/* Yeah, this here KodEz will be used to convert a tree array to
   the corresponding tree.  Standard stuff. */

Node *seq2tree(Node *root, int height)
{
  Node *newNode;

  if (*seqPtr == 0) {
    /* Case Leaf Node */
    newNode = makeNode();
    newNode->key = 0;
    newNode->height = height;
    newNode->father = root;
    seqPtr++;
    return newNode;
  }

  /* Otherwise assume not 0 => 1 */

  newNode = makeNode();
  newNode->key = *seqPtr++;
  newNode->height = height;
  newNode->son[0] = seq2tree(newNode, height + 1);
  newNode->son[1] = seq2tree(newNode, height + 1);

  newNode->father = NULL;
  if(height) newNode->father = root;
  return newNode;
}

Node *com2tree(Node *root, int height, int currX)
{
  Node *newNode;

  // currX is a leaf node
  if ((Left[currX] == 0) && (Right[currX] == 0)) {
    newNode = makeNode();
    newNode->key = 0;
    newNode->height = height;
    newNode->father = root;
    return newNode;
  }

  // otherwise it's not a leaf node
  newNode = makeNode();
  newNode->key = 1;
  newNode->height = height;

  // son[0] is left, son[1] is right
  if (Left[currX] != 0) {
    newNode->son[0] = com2tree(newNode, height+1, Left[currX]);
  }
  if (Right[currX] != 0) {
    newNode->son[1] = com2tree(newNode, height+1, Right[currX]);
  }

  newNode->father = NULL;
  if (height) newNode->father = root;
  return newNode;
}

/* Function to Allocate memory to node */
Node *makeNode( void )
{
  Node *newNode;
  int i;

  newNode = (Node *) malloc(sizeof(Node));
  if(!newNode) {
    printf("Error Allocating Node.  Barf Barf.  Koff Koff.  Vomit. Blehhh\n");
    exit(1);
  }

 for (i=0; i< MAXKIDS; i++) newNode->son[i] = NULL;
 return newNode;
}

/* This routine does an inorder print of the tree */

void inorderWalk(Node *root, DimStruct *dim)
{
  if(!root) return;
  inorderWalk(root->son[0], dim);
  dim->x = max(root->x, dim->x);
  dim->y = max(root->y, dim->y);
  inorderWalk(root->son[1], dim);
  return;
}

/**
**
** This is the Knuth algorithm for tree pretty-printing.
**
** Note this alg is for BINARY treez.
**
**/

void knuthAlg( Node *root)
{
  Node *current;
  int next_number;
  int i=1;

  next_number = 1;
  root->status = ST_FIRST_VISIT;
  current = root;
  while(current) {
    switch (current->status) {
      case ST_FIRST_VISIT : current->status = ST_LEFT_VISIT;
                       if(current->son[0]) {
                         current = current->son[0];
                         current->status = ST_FIRST_VISIT;
                       }
                       break;
      case ST_LEFT_VISIT  : current->x = next_number;
                       next_number += 1;
                       current->y = 2*current->height + 1;
                       current->status = ST_RIGHT_VISIT;
                       if(current->son[1]) {
                         current = current->son[1];
                         current->status = ST_FIRST_VISIT;
                       }
                       break;

      case ST_RIGHT_VISIT : current = current->father;
                       break;
    }
  }

}


/******** ThE aCtuAl GiF drAwInG rOuTine ***********/

void tree2gif(Node *root, DimStruct *dim, int scale_factor, int imgNum) {
  gdImagePtr im;
  FILE *out;
  Node *p;  /* used to walk through the tree */
  char outName[80];
  int black, white, red, notQuiteBlack;

  sprintf(outName, "/theory/www/per/tmpImages/test_%d_%d.gif", PID, imgNum);
  /* Allocate image and colours */
  im = gdImageCreate((dim->x + 1)*scale_factor, (dim->y +1)*scale_factor);
  white = gdImageColorAllocate(im, 255, 255, 255); /* Background */
  black = gdImageColorAllocate(im, 0, 0, 0);
  red = gdImageColorAllocate(im, 255, 0, 0);
  notQuiteBlack = gdImageColorAllocate(im, 0, 0, 1);

  /* Walk through tree and on GIF object */
  process_tree(root, im, scale_factor, white, black, red, notQuiteBlack);

  /* Write to disk and destory image */
  out = fopen(outName, "wb");
  if(!out) {
    printf("DUDE! The fyle didn't open!<BR></TR></TABLE>");
    exit(1);
  }
  gdImageGif(im, out);
  fclose(out);
  gdImageDestroy(im);
}


/*  This routine is used by above to walk thro' tree and
    paint blotches on the image.
*/

void process_tree(Node *root, gdImagePtr im, int scale_factor, int white, int black, int red, int notQuiteBlack) {

  int x1, y1, x2, y2;
  int colour, fillColour;

  if(!root) return;

  x1 = (root->x)*scale_factor;
  y1 = (root->y)*scale_factor;

  /* first draw links, then blotch */

  if(root->son[0]) { /* Internal Node */
    /* Get colours */
    if(root->key == 1) { colour = notQuiteBlack; fillColour = black; }
    if(root->key == 2) { colour = red; fillColour = red; }
    /* Now deal with links to children */
    x2 = (root->son[0]->x)*scale_factor;
    y2 = (root->son[0]->y)*scale_factor;
    gdImageLine(im, x1, y1, x2, y2, black);
    x2 = (root->son[1]->x)*scale_factor;
    y2 = (root->son[1]->y)*scale_factor;
    gdImageLine(im, x1, y1, x2, y2, black);
    /* Draw splotch */
    gdImageArc(im, x1, y1, scale_factor/2, scale_factor/2, 0, 360, colour);
    gdImageFillToBorder(im, x1, y1,  colour, fillColour);
  } else {
    /* Leaf node */
    gdImageFilledRectangle(im, x1 - (scale_factor/6), y1 - (scale_factor/6),x1 +
 (scale_factor/6), y1 + (scale_factor/6), black);
    }

  /* Recursively do this */
  process_tree(root->son[0], im, scale_factor, white, black, red, notQuiteBlack)
;
  process_tree(root->son[1], im, scale_factor, white, black, red, notQuiteBlack)
;
}


#endif
