<HTML>
<HEAD>

<TITLE>The Venn Diagram Page -- Variants</TITLE>

<META NAME="resource-type" CONTENT="document">
<META NAME="description" CONTENT="Variants of Venn diagrams">
<META NAME="keywords" CONTENT="Venn diagram, symmetric Venn diagram, revolving door algorithm, middle two levels problem">
<META NAME="distribution" CONTENT="global">
<META NAME="copyright" CONTENT="Frank Ruskey, 1996.">
<META HTTP-EQUIV="Reply-to" CONTENT="fruskey@csr.uvic.ca">

<BASE=http://theory.cs.uvic.ca/venn/>
</HEAD>

<BODY BGCOLOR="#FFFFFF">
 
<TABLE width=100%>
<TR><TD align=left>
<A HREF=VennEJC.html><IMG border=0 SRC=gifs/smallV3plain.gif></A>
<TD align=right><FONT SIZE=-2>
(to appear in)
<A HREF=http://ejc.math.gatech.edu:8080/Journal/journalhome.html>
<FONT SIZE=-1>T</FONT>HE <FONT SIZE=-1>E</FONT>LECTRONIC
<FONT SIZE=-1>J</FONT>OURNAL OF <FONT SIZE=-1>C</FONT>OMBINATORICS</A>
3 (1996),
<!--
<A HREF=http://ejc.math.gatech.edu:8080/Journal/Surveys/index.html>DS</A>#5
-->
DS #5.
</FONT>
</TR></TABLE>
 
<HR NOSHADE>
 
<CENTER>
<IMG ALIGN=TOP SRC=gifs/VictoriaIcon.gif>
<IMG ALIGN=TOP SRC=gifs/VictoriaIcon.gif>
<H1 ALIGN=CENTER>
<FONT SIZE=-1>The Venn Diagram Page</FONT><BR>
Other Variations 
</H1>
</CENTER>
<! BR CLEAR=ALL>

<HR width="60%">

<H2>Other Types of Venn Diagrams and Extensions</H2>

<P>
Beyond what we've discussed in the preceding pages,
  there are many other aspects of Venn diagrams that have
  been considered in the literature.
We mention some of these below.
 
<H3>The Geometry of Venn Diagrams</H3>

<P>
We've already mentioned some geometrical concerns, such as congruent
  Venn diagrams, and diagrams made from specific curves such as
  circles, ellipses, triangles, and other polygons.

<P>
A Venn diagram is <EM>convex</EM> if the interiors of all
  its curves are convex.
Note that the regions of a Venn diagram can alway be made convex,
  for example, by taking the Tutte embedding of the diagram.
Grunbaum defines a stronger notion of convexity by insisting
  that not only are the curves convex, but also regions and the
  complement of the external region; we call this condition
  <I>strong convexity</I>.

<P>
A Venn diagram is <EM>exposed</EM> if each of its curves touches the
  outer face at some point of non-intersection.
Note, for example, that the 3-Venn diagram #3.2 is not exposed, even
  though all curves have a point of intersection on the outer face.
Stated in terms of <I>n</I>-Venn graphs, being exposed is 
  the same as the condition 
  that the vertex corresponding to the outer face has degree <I>n</I>.
Every symmetric Venn diagram must be exposed (obvious) and
  every convex Venn diagram must be exposed 
  ([<A HREF=VennRefs.html#Gr92a>Gr92a</A>]).
A Venn diagram has a <EM>hidden curve</EM> if it has a curve that
  does not touch the outer region.
For example, in Venn's general construction, curves 4,5,...,<I>n</I>-1
  are all hidden.
Chilakamarri, Hamburger, and Pippert 
  [<A HREF=VennRefs.html#CHP96b>CHP96b</A>] gave an example, shown below,
  of a simple 6-Venn diagram which is not in the same class as
  any exposed diagram.
The hidden curve is the dotted green one.<BR CLEAR=ALL>

<CENTER>
<IMG SRC=gifs/hidden6.gif>
</CENTER>

<P>
Recently, Hamburger and Pippert [<A HREF=VennRefs.html#HP96a>HP96a</A>]
  proved that there are simple reducible Venn diagrams with
  5 congruent ellipses, in spite of the fact that Venn had stated
  that there are no such diagrams.
In fact, there are two of them, but they are in the same class.

<H4>Diagrams made from convex <I>k</I>-gons</H4>

<P>
Let <I>k</I>-gon designate any convex polygon with at most <I>k</I>
  sides.
Define      N(k) to be the maximum number of curves in a Venn diagram
  of whose curves are <I>k</I>-gons.
Gr&uuml;nbaum [Gr75] proves that N(k) is asympotically <I>lg</I>(<I>n</I>)
  via a sqeezing argument, where <I>lg</I> is the base 2 logarithm.
The upper bound uses a construction very much like Edwards' general
  construction, except that the circle is replaced by a square and the
  <I>n</I>th curve is a 2<sup><I>n</I></sup>-gon.
Also, Gr&uuml;nbaum [Gr75, ps 17] states that "the construction we
  used ... is a modification to convex polygons of the method
  described in Moor [<A HREF=VennRefs.html#Mo>Mo</A>]."


<P>
Define <I>K</I>(<I>n</I>) to be the smallest <I>k</I> for which there
  exists a Venn diagram of <I>K</I>(<I>n</I>) curves all of which
  are <I>k</I>-gons.
Gr&uuml;nbaum [Gr75] contains a proof of the following theorem.

<P>
<B>Theorem G.</B> <I>K</I>(3) = <I>K</I>(4) = <I>K</I>(5) = 3, and
  <I>K</I>(6) &lt;= 4.

<P>The value of <I>K</I>(7) is unknown, but Gr&uuml;nbaum has constructed
  and independent family of 7 hexagons.

<H3>Generalizing Venn Diagrams</H3>

We began the "What is a Venn diagram page" with a generalization,
  namely, the independent family, and there are several other
  generalizations that have appeared in the literature.
 
<H4><I>k</I>-fold Venn diagrams</H4>

In an ordinary Venn diagram each intersection consists of one
  connected region.
Fisher, Koh, and Gr&uuml;nbaum [<A HREF=VennRefs.html#FKG>FKG</A>] 
  define a <EM><i>k</i>-fold Venn diagram</EM> to be one 
  in which each intersection consists of exactly <I>k</I> 
  connected regions.
They show how to construct such diagrams for any
  <I>n,k</I> &gt; 1.

<H4><I>d</I>-dimensional Venn diagrams</H4>

Independent sets and Venn diagrams can be generalized to more 
  than two dimensions.
In the definition of independent set and Venn diagram, "curve" gets replaced
  with "boundary of an open <I>d</I>-cell" (that is, homeomorphic
  to a <I>d</I>-sphere) and "region" is replace with "open <I>d</I>-cell".
R&eacute;nyi, R&eacute;nyi, and Sur&aacute;nyi 
  [<A HREF=VennRefs.html#RRS>RRS</A>] proved that an independent family of
  <I>d</I>-spheres has at most <I>d</I>+1 members, thereby generalizing
  the result that a Venn diagram whose curves are circles contains
  at most three curves.

<P>
Gr&uuml;nbaum [<A HREF=VennRefs.html#Gr75>Gr75</A>] mentions several 
  general geometric problems on <I>d</I>-dimensional diagrams where the
  sets are required to be boundaries of <I>d</I>-polytopes or
  convex regions.

<P>
<HR NOSHADE>
 
<TABLE width=100%>
<TR><TD align=left>
<A HREF=VennEJC.html><IMG border=0 SRC=gifs/smallV3plain.gif></A>
<TD align=right><FONT SIZE=-2>
(to appear in)
<A HREF=http://ejc.math.gatech.edu:8080/Journal/journalhome.html>
<FONT SIZE=-1>T</FONT>HE <FONT SIZE=-1>E</FONT>LECTRONIC
<FONT SIZE=-1>J</FONT>OURNAL OF <FONT SIZE=-1>C</FONT>OMBINATORICS</A>
3 (1996),
<!--
<A HREF=http://ejc.math.gatech.edu:8080/Journal/Surveys/index.html>DS</A>#5
-->
DS #5.
</FONT>
</TR></TABLE>
 
</BODY>
</HTML>

