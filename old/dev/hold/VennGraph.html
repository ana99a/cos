<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<HTML>
<HEAD>

<TITLE>The Venn Diagram Page: Graphs</TITLE>

<META NAME="resource-type" CONTENT="document">
<META NAME="description" CONTENT="The Venn Diagram Page: Graphs, isomorphism classes">
<META NAME="keywords" CONTENT="Venn diagrams, planar graphs, symmetric Venn diagrams">
<META NAME="distribution" CONTENT="global">
<META NAME="copyright" CONTENT="Frank Ruskey, 1996.">
<META HTTP-EQUIV="Reply-to" CONTENT="fruskey@csr.uvic.ca">

<BASE=https://webhome.cs.uvic.ca/~cos/venn/>
</HEAD>

<BODY>

<A HREF=Venn.html><IMG SRC=gifs/smallV3plain.gif></A>
<HR>

<CENTER>
<IMG ALIGN=TOP SRC=gifs/VictoriaIcon.gif>
<IMG ALIGN=TOP SRC=gifs/VictoriaIcon.gif>
<H1 ALIGN=CENTER>
The Venn Diagram Page<BR>
Graphs Associated with Venn Diagrams 
</H1>
</CENTER>
<BR CLEAR=ALL>

<H2>The Planar Dual of a Venn Diagram</H2>

<P>
An <I>n</I>-Venn diagram <B>C</B> = {C<sub>1</sub>,...,C<sub><I>n</I></sub>}
  may be regarded as a graph in two ways.
Firstly, the diagram itself can be thought of as a edge-colored 
  plane<SUP><A HREF=VennFoot.html#plane>5</A></SUP> graph V(<B>C</B>)
  whose vertices correspond to intersections of curves, and whose
  edges correspond to the segments of curves
  between two adjacent vertices.   
Edges are colored according the the curve to which they belong.
Following [<A HREF=VennRefs.html#CHP95>CHP</A>], 
  we overload the term and call this graph the <I>Venn diagram</I>.
In an <EM>unlabelled</EM> Venn diagram we ignore the edge labels.
As and example, the three circle Venn diagram 
  has 6 vertices (corresponding to the 6 intersections) and 12 edges.
Recall Euler's formula <I>f</I> = <I>e - v</I> + 2 relating
  the number of faces, edges, and vertices of a graph
  embedded in the plane.
Assuming that no three curves intersect at a common point
  (i.e., a simple Venn diagram), we have <I>e</I> = 2<I>v</I>.
By definition the number of faces is <I>f</I> = 2<sup><I>n</I></sup>.
It thus follows that <I>v</I> = 2<sup><I>n</I></sup> - 2 and
  <I>e</I> = 2<sup><I>n</I>+1</sup> - 4 for simple Venn diagrams.

<P>
Two curves can meet at a point <EM>transversally</EM> or not, depending
  on whether the two curves cross.
A moments reflection will convince you that the curves in a
  simple Venn diagram must meet transversally.
More generally, at any point of intersection in a Venn diagram, 
  there must be at least two curves that meet transversally.

<P>
With each Venn diagram, <B>C</B>, we may associate another plane graph
  called the <EM>Venn graph</EM>, and denoted G(<B>C</B>), 
  which is the planar dual of the Venn diagram.
It's vertices are the connected open regions (faces) from
  the definition of Venn diagrams.
Two vertices are connected by an edge if they share a common boundary.
By default the edges are colored (by the color of the corresponding
  edge in the Venn diagram); if not, it is called an
  unlabelled Venn graph.
For example, the unlabelled Venn graph of the three circle Venn 
  diagram is an embedding of the 3-cube, <I>Q</I><sub>3</sub>.
Clearly, every <I>n</I>-Venn graph is a planar spanning subgraph of the
  <I>n</I>-cube, <I>Q</I><sub><I>n</I></sub>.
If <B>C</B> is a simple Venn diagram, then every face of G(<B>C</B>)
  is a quadrilateral, and thus is a maximal bipartite planar
  graph. 

The following list contains illustrations of some unlabelled Venn graphs.

<UL>
<LI><A HREF=VennGeneral.html>Venn's general construction</A> 
  (for <I>n</I>=3,4,5).
<LI><A HREF=gifs/Gray5graph.gif>Edwards' general construction for 
  <I>n</I>=5</A>.
</UL>

<H3>When are two Venn diagrams different?</H3>

Two Venn diagrams are <EM>isomorphic</EM> if, by a continuous
  transformation of the plane, one of them can be changed into
  the other or its mirror image.
Two Venn diagrams are <EM>equivalent</EM> if they are isomorphic
  as plane graphs, ignoring the edge colors
  (that is, isomorphic as planar graphs embedded in the plane).
Since curves meet transversally in simple Venn diagrams, two
  simple Venn diagrams are isomorphic if and only if they are
  equivalent.

<P>
One the other hand, a Venn diagram may be embedded in the sphere
  via stereographic projection.
In some sense it is more natural to look at Venn diagrams 
  as being embedded on the sphere.
Two Venn diagrams are <EM>in the same class</EM> if they can be
  projected to the same spherical Venn diagram.

<H3>Extending Venn diagrams</H3>

<P>
A Venn diagram is <EM>reducible</EM> if the removal of one of its
  <I>n</I> curves results in a Venn diagram with <I>n</I>-1 curves.
The two general constructions we presented both yield reducible diagrams.
A Venn diagram that is not reducible is <EM>irreducible</EM>.
The ellipse diagram shown below <A HREF=irreduce.html>is irreducible</A>.

<P>
A <I>n</I>-Venn diagram <B>C</B> is <EM>extendible</EM> if 
  there is a curve that
  can be added to it to make an (<I>n</I>+1)-Venn diagram,
  <B>C'</B>.
Diagram <B>C'</B> is said to be an <EM>extension</EM> of <B>C</B>.
Winkler [<A HREF=VennRefs.html#Wi>Wi</A>] makes the following two
  observations:

<P>
<B>Theorem W1</B> For <I>n</I> greater than one, an <I>n</I>-Venn diagram
  <B>C</B> is extendible to an (<I>n</I>+1)-Venn diagram if and only if
  its Venn graph G(<B>C</B>) is Hamiltonian.

<P>
<B>Theorem W2</B> If the <I>n</I>-Venn diagram <B>C</B> is an extension
  of an (<I>n</I>-1)-Venn diagram, then <B>C</B> is extendible to an
  (<I>n</I>+1)-Venn diagram.

<P>
To prove the later theorem, let <B>B</B> be the diagram whose extension is  
  <B>C</B>.
By Theorem W1, The curve H added to <B>B</B> to get <B>C</B> 
  corresponds to a Hamiltonian cycle in G(<B>B</B>).  
In G(<B>C</B>), curve H becomes the 
  prism<sup><A HREF=VennFoot.html#prism>4</A></sup>, 
  P, of a cycle of length 2<sup>n</sup>.
Since the prism of a cycle is Hamiltonian, <B>C</B> is extendible.

<P>
Edwards general construction is a manifestation of this proof of
  Theorem W2, where the Hamilton cycle in P is the one that alternates
  back and forth between the two copies of the cycle, two vertices at
  a time from each cycle.
Venn's general construction is related but different since it uses
  non-prism edges. 

<H3>How many are there?</H3>

<P>
The precise number of Venn diagrams and Venn classes has been determined
  by Hamburger and Pippert 
  [<A HREF=VennRefs.html#HP96b>HP96b</A>, 
  <A HREF=VennRefs.html#HP96c>HP96c</A>] 
  for <I>n</I> = 1,2,3,4,
  and reducible simple diagrams and classes for <I>n</I> = 5.
For <I>n</I> = 1,2 clearly there is only one diagram.

<H4><I>n</I> = 3</H4>

<P>
For <I>n</I> = 3 there are six distinct Venn classes and fourteen distinct
  Venn diagrams.  Only one of the diagrams is simple (Class 6).
<UL>
<LI><A HREF=gifs/venn3c-01-02.gif>Class 1</A> (diagrams #3.1,#3.2);
  with <A HREF=gifs/CO-venn3c-01-02.gif>regions colored</A>. 
<LI><A HREF=gifs/venn3c-03-04.gif>Class 2</A> (diagrams #3.3,#3.4); 
  with <A HREF=gifs/CO-venn3c-03-04.gif>regions colored</A>. 
<LI><A HREF=gifs/venn3c-05-06.gif>Class 3</A> (diagrams #3.5,#3.6); 
  with <A HREF=gifs/CO-venn3c-05-06.gif>regions colored</A>. 
<LI><A HREF=gifs/venn3c-07-08-09-10.gif>Class 4</A> 
  (diagrams #3.7,#3.8,#3.9,#3.10); 
  with <A HREF=gifs/CO-venn3c-07-08-09-10.gif>regions colored</A>. 
<LI><A HREF=gifs/venn3c-11-12-13.gif>Class 5</A> (diagrams #3.11,#3.12,#3.13); 
  with <A HREF=gifs/CO-venn3c-11-12-13.gif>regions colored</A>. 
<LI><A HREF=#3circle>Class 6</A> (diagram #3.14 
  -- the three circle diagram shown above)</A>; 
  with <A HREF=#3circle>regions colored</A>.
</UL>

<P>
Note that classes 2 and 3 are equivalent but not isomorphic, as are
  diagrams #3.3 & #3.6 (and #3.4 & #3.5).
It is interesting that diagrams #3.3 & #3.6 look identical with curves
  drawn in black and regions colored, even though the corresponding
  Venn diagrams are different.
Diagram #3.3 appears also in 
  [<A HREF=VennRefs.html#ES>ES</A>] (showing that it is a convex diagram).
The diagrams in Class 2 have the property that each pair of curves
  meet tranversally; the diagrams of Class 3 do not have this property.

<H4><I>n</I> > 3</H4>

<P>
For <I>n</I> = 4 there are two distinct simple Venn diagrams 
  and one distinct Venn class (this was first observered by
  Gr&uuml;nbaum 
  [<A HREF=VennRefs.html#Gr92a>Gr92a</A>,p.8]).

<P>
For <I>n</I> = 5 there are 11 distinct, reducible, simple Venn classes and
  12 distinct, reducible, simple Venn diagrams.
Drawings of these may be found in 
  [<A HREF=VennRefs.html#HP96c>HP96c</A>].

<P>
In proving these results several theorems of independent interest were proven:

<P>
<B>Theorem</B>
A simple Venn diagram for <I>n</I> &gt; 2 is 3-connected. 

<P>
A theorem of Whitney [<A HREF=VennRefs.html#Wh>Wh</A>] states that
  a plane embedding of a 3-connected planar graph is unique, once the
  outer face has been identified.
Thus, to determine whether two diagrams are in the same class, we need
  only tray all possible embedings of one of them with each of its faces 
  as the outer region and observe whether any of these embeddings is 
  isomorphic to the other diagram.

<P>
<B>Theorem</B>
A simple Venn graph for <I>n</I> &gt; 2 is 3-connected.

<P>
<B>Theorem</B>
No two faces of a Venn diagram belong to the same curve.

<H2>Venn diagrams made from congruent curves</H2>

The general constructions outlined above do not share a nice property
  of the first two figures on the "What is a Venn Diagram" page, 
  namely that they are constructed from
  congruent<sup><A HREF=#congruent>2</A></sup> curves. 
In fact Gr&uuml;nbaum 
  [<A HREF=VennRefs.html#Gr92b>Gr92b</A>] 
  defines a Venn diagram to be <EM>nice</EM> 
  if it is made from congruent curves, but we'll prefer to
  call it a congruent Venn diagram.

<P>
<CENTER>
<IMG SRC=gifs/ellipse2.gif ALIGN=LEFT>
<BR CLEAR=RIGHT>
</CENTER>
 
<P CLEAR=RIGHT>
Here we show a beautiful congruent Venn diagram made from 5
  congruent ellipses.  
The first such diagrams were constructed by Gr&uuml;nbaum [?].
The diagram above labels each region (except the very smallest, 
  where the labels wouldn't fit) with the labels of all ellipses 
  that contain the region.
Below the regions are colored according to the number
  of ellipses in which they are contained: grey = 0, yellow = 1,
  red = 2, blue = 3, green = 4, and black = 5.  
Note that the number of regions colored with a given color corresponds to
  the appropriate binomial coefficient: #(grey) = #(black) = 1,
  #(yellow) = #(green) = 5, and #(red) = #(blue) = 10.

<P>
<CENTER>
<IMG SRC=gifs/ellipsevenn.gif ALIGN=RIGHT>
<BR CLEAR=RIGHT>
</CENTER>

<H2>Venn diagrams and "Revolving Door" lists</H2>

Note that some of the blue regions in the ellipse diagram above are
  "adjacent" in the sense that their boundary shares a common
  point.
The sets corresponding to these regions differ by one element.
The point of intersection acts like a revolving door; one element
  is removed from the subset, another element is added.
Sometimes these adjacent regions form a cycle, as in the diagram
  above.
In this case we have a revolving door listing of the <I>k</I>-subsets
  of <I>n</I>.
Below we list the subsets corresponding to the 1,2,3,4-subsets
  (yellow, red, blue, green) in the above diagram.

<P>
<CENTER>
<TABLE BORDER=1 CELLPADDING=5 CELLSPACING=1><TR>
<TD ALIGN=CENTER WIDTH=60>1
<TD ALIGN=CENTER WIDTH=60>2
<TD ALIGN=CENTER WIDTH=60>3
<TD ALIGN=CENTER WIDTH=60>4
<TD ALIGN=CENTER WIDTH=60>5
</TABLE>
<TABLE BORDER=1 CELLPADDING=5 CELLSPACING=1><TR>
<TD ALIGN=CENTER WIDTH=40>12
<TD ALIGN=CENTER WIDTH=40>13
<TD ALIGN=CENTER WIDTH=40>35
<TD ALIGN=CENTER WIDTH=40>25
<TD ALIGN=CENTER WIDTH=40>45
<TD ALIGN=CENTER WIDTH=40>14
<TD ALIGN=CENTER WIDTH=40>34
<TD ALIGN=CENTER WIDTH=40>35
<TD ALIGN=CENTER WIDTH=40>23
<TD ALIGN=CENTER WIDTH=40>24
</TABLE>
<TABLE BORDER=1 CELLPADDING=5 CELLSPACING=1><TR>
<TD ALIGN=CENTER WIDTH=40>123
<TD ALIGN=CENTER WIDTH=40>135
<TD ALIGN=CENTER WIDTH=40>125
<TD ALIGN=CENTER WIDTH=40>245
<TD ALIGN=CENTER WIDTH=40>145
<TD ALIGN=CENTER WIDTH=40>134
<TD ALIGN=CENTER WIDTH=40>345
<TD ALIGN=CENTER WIDTH=40>235
<TD ALIGN=CENTER WIDTH=40>234
<TD ALIGN=CENTER WIDTH=40>124
</TABLE>
<TABLE BORDER=1 CELLPADDING=5 CELLSPACING=1><TR>
<TD ALIGN=CENTER WIDTH=60>1234
<TD ALIGN=CENTER WIDTH=60>1235
<TD ALIGN=CENTER WIDTH=60>1245
<TD ALIGN=CENTER WIDTH=60>1345
<TD ALIGN=CENTER WIDTH=60>2345
</TABLE>
</CENTER>

<H3>A Rotated Version of Edwards' Construction and Counting in Binary</H3>

Suppose that we rotate the <I>j</I>th curve in Edwards' general
  construction by <B>pi</B>/<I>j</I> radians.
The result is still a Venn diagram, but it is no longer simple.

<UL>
<LI><I>n</I> = 3:
  <A HREF=gifs/rot3.gif>black and white</A>,
  <A HREF=gifs/rot3c.gif>colored</A>.
<LI><I>n</I> = 4: 
  <A HREF=gifs/rot4.gif>black and white</A>, 
  <A HREF=gifs/rot4c.gif>colored</A>.
<LI><I>n</I> = 5: 
  <A HREF=gifs/rot5.gif>black and white</A>, 
  <A HREF=gifs/rot5c.gif>colored</A>.
<LI><I>n</I> = 6: 
  <A HREF=gifs/rot6.gif>black and white</A>, 
  <A HREF=gifs/rot6c.gif>colored</A>.
<LI><I>n</I> = 7: 
  <A HREF=gifs/rot7.gif>black and white</A>, 
  <A HREF=gifs/rot7c.gif>colored</A>.
<LI><A HREF=gifs/anim-rot.gif>animated</A> for <I>n</I>=3..7.
</UL>

Aside from being attractive, the rotated version illustrates an interesting
connection between the binary reflected Gray code and counting in binary.
The binary reflected Gray code arises an a Hamilton path
  following two circles (<A HREF=VennGray.html#gray>picture</A>)in the Venn
  graph of Edwards original construction.
In the rotated version, with a minor modification, these same two circles
  may be traversed to count in binary (<A HREF=VennGray.html#count>picture</A>).

<P>
A scan of Edwards' rotatable Venn diagram manuscript:
  <A HREF=gifs/scan-ed1.gif>page 1</A>,
  <A HREF=gifs/scan-ed1.gif>page 2</A>,
  <A HREF=gifs/scan-ed1.gif>page 3</A>,
  <A HREF=gifs/scan-ed1.gif>page 4</A>.

<H2>Monotone Venn diagrams</H2>

A Venn diagram is <EM>monotone</EM> if the regions (and their
  boundaries) corresponding to all the <I>k</I>-subsets
  are connected in a cycle.
<!-- This is my definition and term; perhaps there is something better -->
The general constructions above are monotone.
Below we will see an examples of non-monotone Venn diagrams.

<P>
The above was a rather imprecise definition, meant to capture our intuition
  based on the illustrations of symmetric diagrams.
Let's try to be more precise.
The dual graph, or Venn graph as defined above, could also have been
  defined as a directed graph, with the direction indicating whether one
  vertex was a subset of another.
In this sense, the Venn graph becomes a spanning subgraph of the Hasse
  diagram (regarded as a direct graph) of the subset lattice.
Let's call this the <EM>directed Venn graph</EM>. 
A Venn diagram is <EM>monotone</EM> if the directed Venn graph has
  a unique maximum element and a unique minimum element.

<H2>Winkler's Conjecture</H2>

Winkler [<A HREF=VennRefs.html#Wi>Wi</A>] conjectured that every
  simple Venn diagram of <I>n</I> curves can be extended to a 
  simple Venn diagram of <I>n</I>+1 curves by the addition of a suitable curve.
This conjecture was generalized  by Gr&uuml;nbaum 
  [<A HREF=VennRefs.html#Gr92b>Gr92b</A>] by removing the restriction
  of simplicity.
Gr&uuml;baum's version was proven by Chilakamarri, Hamburger, and Pippert
  [<A HREF=VennRefs.html#CHP96>CHP96</A>], but the restricted problem remains open.

<P>
The proof of 
  [<A HREF=VennRefs.html#CHP96>CHP96</A>] 
  makes use of the <EM>radual graph</EM>, R(<B>C</B>) of the
  Venn diagram, which, for an arbitrary plane graph,
  is the union of the radial graph and the dual graph (see Ore
  [<A HREF=VennRefs.html#Or>Or</A>]).
A simple example, using Venn diagram #3.4 is shown 
  <A HREF=gifs/radual.gif>here</A>.
The strategy of their proof is first to show that R(<B>C</B>) is
  a simple triangulation and then invoke a theorem of Whitney 
  [<A HREF=VennRefs.html#Wh>Wh</A>] that
  any such graph is Hamiltonian.
It is then easy to see that the Hamilton cycle in the radual graph can be
  used as the additional curve.

<HR>
<A HREF=Venn.html><IMG SRC=gifs/smallV3plain.gif></A>
<HR>

<FONT SIZE=-1>
This page created by <A HREF=mailto:fruskey@csr.uvic.ca>Frank Ruskey</A>, 
  May 1996.<BR>
It was last updated <!--#echo var="LAST_MODIFIED" -->.<BR>
<!--#exec cgi="inc/cos-counter/counter-nl"--><BR>
&copy; Frank Ruskey, 1996.<BR>
</FONT>

</BODY>
</HTML>

