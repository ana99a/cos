/* Generating Numerical Partitions */

#include <stdio.h>
#include <string.h>

int N;  /* partitions of integer N */
int K;  /*   into K parts          */
int M;  /*   with largest part M   */
int count = 0;

int p[100]; /* stores the partition */
int hist[100];

void PrintIt( int t )
{ int i;
  for (i=2; i<=t; ++i) {
     /* printf( " %d", p[i] ); */
     ++hist[p[i]]; 
  }
  /* printf( "\n" ); */
}

int min ( int x, int y ) { return( x<y?x:y ); }

/* Partitions of n with largest part k */
void PartA ( int n, int k, int t )
{ int j;
  p[t] = k;
  if (n == k || k == 1) PrintIt( t+n-k );
  else for (j=1; j<=min(k,n-k); j++) PartA( n-k, j, t+1 );
  p[t] = 1;
}

void main (int argc, char *argv[])
{ int i,r,c;
  printf( "<HTML>\n" );
  printf( "<H1>Number of parts of size <I>k</I> among all partitions of <I>n</I></H1>\n" );
  printf( "<TABLE cellpadding=3 cellspacing=0 border=0>\n<TR>\n" );
  printf( "<TH bgcolor=orange><I>n</I>/<I>k</I></TH>" );
  for (N=1; N<21; ++N) printf( "<TH bgcolor=yellow>%d</TH>", N );
  for (N=1; N<41; ++N) {
     for (i=1; i<=N+1; i++) p[i] = 1;
     for (i=1; i<=N+1; i++) hist[i] = 0;
     count = 0;  
     PartA( 2*N, N, 1 );
     printf( "<TR><TH bgcolor=yellow>%d</TH>\n", N );
     for (c=1; c<=min(N,20); ++c) {
        printf( "<TD align=right>%d</TD>", hist[c] );
     }
     printf( "</TR>\n" );
     /*
     for (c=1; c<=N; ++c) {
        printf( " %5d", hist[c] );
     }
     printf( "\n" );
     for (c=1; c<=N; ++c) {
        printf( " %1d", hist[c]%2 );
     }
     printf( "\n" );
     */
  }
  printf( "</TABLE>\n" );
  printf( "</HTML>\n" );
  exit( 0 );
}


