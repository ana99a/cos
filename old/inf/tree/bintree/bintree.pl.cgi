#!/usr/bin/perl
# This script will produce an animation on users' browser.
# To creat an animation, you need to make a directory that contains
# animation sequences, jpeg files.  Then, make a soft link to
# this script in the directory.  When the link is activated, 
# animation.script will be read to determine how the animation should
# be played.
#
# The animation.script should contain:
#   frame whatever        (must be the name of the picture)
#   frametype jpg         (or gif)
#   start n               (start frame)
#   end n                 (finish frame)
#   interpolate n1 n2 ... (seconds to wait between frames)
#   loop n                (number of times to loop, 0 to loop forever)
#   alt filename          (displayed if browser is not netscape)

$script = "animation.script";

# read the script file
-e $script || die "animation script not found";
open(SCRIPT,$script);
while (<SCRIPT>) {
    ($tag,@value) = split;
    $control{$tag} = join(' ',@value);
}
@channel = split(' ',$control{'interpolate'});

$divider = 'animation';
$cgiheader = "Content-type: multipart/x-mixed-replace;boundary=$divider\n";
$imageheader = "Content-type: image/$control{'frametype'}\n\n";
$blockstart = "\n--$divider\n";
$blockend = "\n--$divider--\n";

if ($control{'frametype'} eq 'jpeg') {
    $control{'frametype'} = 'jpg';
}

$_ = $ENV{'HTTP_USER_AGENT'};
($browser,@_) = split('/');

sub sendfile {
    local($filename) = @_;
    $/ = 0;
    $| = 1;
    open(FILE,$filename);
    while (<FILE>) {
	print;
    }
    close(FILE);
}

$browser = 'Mozilla';

MAIN: {
    if ($browser ne 'Mozilla') {
	print $imageheader;
	&sendfile($control{'alt'});
	exit 0;
    } else {
	print $cgiheader;

	if (! $control{'loop'}) {
	    while (1) {
		for ($i = $control{'start'}; $i <= $control{'end'}; $i++) {
		    sleep($channel[$i-$control{'start'}]);
		    print $blockstart;
		    print $imageheader;
		    &sendfile($control{'frame'}."$i.".$control{'frametype'});
		}
	    }
	}

	for ($loop = 0; $loop < $control{'loop'}; $loop++) {
	    for ($i = $control{'start'}; $i <= $control{'end'}; $i++) {
		sleep($channel[$i-$control{'start'}]);
		print $blockstart;
		print $imageheader;
		&sendfile($control{'frame'}."$i.".$control{'frametype'});
	    }
	}
	print $blockend;
	exit 0;
    }
}
