/* This is file PentShow.c */
/* Copyright Frank Ruskey, 1996 */
/* "Ownership": Frank Ruskey */
/* Original version programmed by G.L. Chen */
/* Modified version programmes by Q. O. Liddicoat */
#include <stdio.h>
#include <stdlib.h>
#include "e_set.h"

#define NUM_PIECES	12
#define MAX_RC		20
#define xMAX_RC		21
#define INPUT_NAME	"./e_BigPent.temp"
#define DATA_NAME	"./e_610PDATA.TEXT"
#define RxC			79
#define MAXBOARD 64
#define ICONDIR "./Ico/"

typedef long PieceNumber;
typedef unsigned long BoardNumber;
typedef set Board;
typedef struct ListElement {
	Board Position;
	struct ListElement *Link;
} ListElement;
typedef struct _REC_Piece {
	int X, Y;
} _REC_Piece;
typedef struct LOC_Isolated {
	char zeroes;
} LOC_Isolated;

int ROW, COL;
long xROW, xCOL, LIMIT, Min_Square;
long minX, maxX, minY, maxY;
ListElement *List[NUM_PIECES][RxC + 1];
_REC_Piece Piece[NUM_PIECES][5];
int n, num, count;

ListElement *p;
char ch;
Board PieceAvail;
ListElement *Solution[NUM_PIECES];
Board TheBoard, TrialPiece;
int WriteOutLists;
long A[xMAX_RC + 1][xMAX_RC + 1];
char board[MAXBOARD][MAXBOARD];
int M,N,count, format;

void Translate(PieceNumber pc) {
	int i, j, xt, yt, sq;
	BoardNumber Part[5];
	BoardNumber min;
	ListElement *ptr;
	minX = 9999;	minY = 9999;
	maxX = -9999;	maxY = -9999;
	for (j = 0; j < 5; j++) {
		if ( Piece[pc - 1][j].X < minX ) minX = Piece[pc - 1][j].X;
		if ( Piece[pc - 1][j].Y < minY ) minY = Piece[pc - 1][j].Y;
		if ( Piece[pc - 1][j].X > maxX ) maxX = Piece[pc - 1][j].X;
		if ( Piece[pc - 1][j].Y > maxY ) maxY = Piece[pc - 1][j].Y;
	}
	for (i = -minX; i < COL - maxX; i++) {
		for (j = -minY; j < ROW - maxY; j++) {
			for (sq = 0; sq < 5; sq++) {
				xt = i + Piece[pc - 1][sq].X;
				yt = j + Piece[pc - 1][sq].Y;
				Part[sq] = yt + xt*ROW;
				if ((Part[sq] > RxC) || (Part[sq] < 0)) printf("oops");
			}
			TrialPiece = e_init();
			e_add5(Part[0], Part[1], Part[2], Part[3], Part[4], &TrialPiece);
			if (e_isempty(e_intersect(TrialPiece, TheBoard))) {
				min = Part[0];
				for (sq = 1; sq < 5; sq++) {
					if (Part[sq] < min)
						min = Part[sq];
				}
				num++;
				ptr = (ListElement*)malloc(sizeof(struct ListElement));
				ptr->Position = e_copy(TrialPiece);
				ptr->Link = List[pc - 1][min];
				List[pc - 1][min] = ptr;
			}
		}
	}
}

void Rotate(PieceNumber pc) {
	int j, temp;
	for (j = 0; j < 5; j++) {
		temp = Piece[pc - 1][j].X;
		Piece[pc - 1][j].X = Piece[pc - 1][j].Y;
		Piece[pc - 1][j].Y = -temp;
	}
}	

void Flip(PieceNumber pc) {
	int j;
	for (j = 0; j < 5; j++)
		Piece[pc - 1][j].X = -Piece[pc - 1][j].X;
}

void WriteOut(PieceNumber pc) {
	long ii;
	BoardNumber bd;
	n = 0;
	p = (ListElement*)malloc(sizeof(struct ListElement));

	if (WriteOutLists) printf("pc = %8d\n", pc);
	for (bd = 0; bd <= RxC; bd++) {
		if (WriteOutLists) { printf("  bd = %8d\n", bd); }
		p = List[pc - 1][bd];
		while (p != NULL) {
			if (WriteOutLists) {
				for (ii = 0; ii <= RxC; ii++) {
					if (e_inset(ii, p->Position))
						printf("%4d",ii);
				}
				printf("\n");
			}
			n++;
			p = p->Link;
		}
	}
	if (WriteOutLists) printf("piece = %d\tn = %d\n", pc, n);
}

void GeneratePiecePositions() {
	PieceNumber pc;
	BoardNumber bd;
	set tmp, tmp2;
	tmp = e_init();
	tmp2 = e_init();
	e_add5(3, 4, 8, 9, 11, &tmp);
	tmp = e_add_to_set(tmp, 12); /* 3, 4, 8, 9, 11, 12 */
	tmp2 = e_add_to_set(tmp2, 2);/* 2, 4 */
	tmp2 = e_add_to_set(tmp2, 4);
	for (pc = 1; pc <= NUM_PIECES; pc++) 
		for (bd = 0; bd <= RxC; bd++)
			List[pc - 1][bd] = NULL;
	Translate(1);	
	WriteOut(1);
	for (pc = 2; pc <= NUM_PIECES; pc++) {
		for (bd = 0; bd <= RxC; bd++) List[pc - 1][bd] = NULL;
		Translate(pc);	Rotate(pc);
		Translate(pc);	Rotate(pc);
		if (!e_inset(pc, tmp2)) { /* 2, 4 */
			Translate(pc);	Rotate(pc);
			Translate(pc);	Rotate(pc);
		}
		if (e_inset(pc, tmp)) { /* 3, 4, 8, 9, 11, 12 */
			Flip(pc);
			Translate(pc);	Rotate(pc);
			Translate(pc);	Rotate(pc);
			if (!e_inset(pc, tmp2)) { /* 2, 4 */
				Translate(pc);	Rotate(pc);
				Translate(pc);	Rotate(pc);
			}
			Flip(pc);
		}
		WriteOut(pc);
	}
}

int FindStartRow() {
    int i, j;
    /* skip blank lines */
	for (i=0; i< N; i++)
		for (j=0; j< M; j++)
			if (board[i][j] != '.') return(i);
    return(-1);
}

int FindStartCol() {
    int i, j;
    /* skip blank lines */
    for (j=0; j< M; j++)
		for (i=0; i< N; i++)
			if (board[i][j] != '.') return(j);
    return(-1);
}

int FindEndRow() {
    int i, j;
    /* skip blank lines */
    for (i=(N-1); i >=0; i--)
		for (j=0; j< M; j++)
			if (board[i][j] != '.') return(i);
    return(-1);
}

int FindEndCol() {
    int i, j;
    /* skip blank lines */
    for (j=(M-1); j >=0; j--)
		for (i=0; i< N; i++)
			if (board[i][j] != '.') return(j);
    return(-1);
}

void PrintRow (int format) {
    int r,c;
    int SR, SC, ER, EC;
    char cell,right,down, cross;

    SR = FindStartRow();
    SC = FindStartCol();
    ER = FindEndRow();
    EC = FindEndCol();

    if (format & 1) {  /* user want the letter matrix output */
	printf("<td><pre>\n");
	for (r = SR; r <= ER; r++) {
	    for (c = SC; c <= EC; c++) {
		printf("%c",((board[r][c]=='.')? ' ':board[r][c]));
	    }
	    printf("\n");
	}
	printf("</pre></td>");
    }

    if (format & 2) {    /* user want the pretty picture */
	printf("<td><table border=0 cellpadding=0 cellspacing=0 >\n");
	
	/* prints a square with a dot on lower right */
	printf("<tr><td>");
	if (board[SR][SC] != '.')
	    printf("<img src=\"%stsplined.gif\" border=0 height=30 width=30>",ICONDIR);
	printf("</td>\n");

	/* if there is an adjacent piece below, print a lower line */
	for (c=SC; c<=EC; c++) {
	    printf("<td>");
	    if (board[SR][c] != '.')
		printf("<img src=\"%stsplineb.gif\" border=0 height=30 width=30>",ICONDIR);
	    else if ((c<EC) && (board[SR][c+1] != '.'))
		printf("<img src=\"%stsplined.gif\" border=0 width=30 height=30>",ICONDIR);
	    printf("</td>\n");
	}
	printf("</tr>");

	/* now print the whole thing */
	for (r = SR; r <= ER; r++) {

	    /* print the first border */
	    printf("<tr><td>");
	    if (board[r][SC] != '.')
		printf("<img src=\"%stspliner.gif\" border=0 height=30 width=30>",ICONDIR);
	    else if ((r<ER) && (board[r+1][SC] != '.'))
		printf("<img src=\"%stsplined.gif\" border=0 height=30 width=30>",ICONDIR);
	    printf("</td>\n");

	    /* now the actual pieces */
	    for (c = SC; c <= EC; c++) {
		printf("<td>");
		cell = board[r][c];
		right = board[r][c+1];
		down = board[r+1][c];
		cross = board[r+1][c+1];

		if (cell == '.') {
		    if ((r == ER) && (c == EC)) {

		    } else if (c == EC) {
			if (cell != down) 
			    printf("<img src=\"%stsplineb.gif\" border=0 height=30 width=30>",ICONDIR);

		    } else if (r == ER) {
			if (cell != right) 
			    printf("<img src=\"%stspliner.gif\" border=0 height=30 width=30>",ICONDIR);

		    } else if (cell != right) {
			if (cell != down) 
			    printf("<img src=\"%stsplinerb.gif\" border=0 height=30 width=30>",ICONDIR);
			else printf("<img src=\"%stspliner.gif\" border=0 height=30 width=30>",ICONDIR);
		    } else {
			if (cell != down) 
			    printf("<img src=\"%stsplineb.gif\" border=0 height=30 width=30>",ICONDIR);
			else {
			    if (cell != cross)
				printf("<img src=\"%stsplined.gif\"border=0 height=30 width=30>",ICONDIR);
		      /*    else printf("<img src=\"%stspline.gif\"border=0 height=30 width=30>",ICONDIR);*/
			}
		    }
		}
		else {
		    if ((r == ER) && (c == EC)) {
			printf("<img src=\"%sgreenrb.gif\" border=0 height=30 width=30>",ICONDIR);
		    } else if (c == EC) {
			if (cell != down) 
			    printf("<img src=\"%sgreenrb.gif\" border=0 height=30 width=30>",ICONDIR);
			else printf("<img src=\"%sgreenr.gif\" border=0 height=30 width=30>",ICONDIR);
		    } else if (r == ER) {
			if (cell != right) 
			    printf("<img src=\"%sgreenrb.gif\" border=0 height=30 width=30>",ICONDIR);
			else printf("<img src=\"%sgreenb.gif\" border=0 height=30 width=30>",ICONDIR);
		    } else if (cell != right) {
			if (cell != down) 
			    printf("<img src=\"%sgreenrb.gif\" border=0 height=30 width=30>",ICONDIR);
			else printf("<img src=\"%sgreenr.gif\" border=0 height=30 width=30>",ICONDIR);
		    } else {
			if (cell != down) 
			    printf("<img src=\"%sgreenb.gif\" border=0 height=30 width=30>",ICONDIR);
			else {
			    if (cell != cross)
				printf("<img src=\"%sgreend.gif\" border=0 height=30 width=30>",ICONDIR);
			    else printf("<img src=\"%sgreen.gif\" border=0 height=30 width=30>",ICONDIR);
			}
		    }
		}
		printf("</td>\n"); 
	    }
	    printf("</tr>\n");
	}

	printf("</table></td>");
    }  /* end of pretty picture */
}

char conv( long nn )
{
	char result;
	switch (nn) {
		case -1: result = '.'; break;
		case 0:  result = '.'; break;
		case 1:  result = 'X'; break;
		case 2:  result = 'I'; break;
		case 3:  result = 'F'; break;
		case 4:  result = 'Z'; break;
		case 5:  result = 'W'; break;
		case 6:  result = 'T'; break;
		case 7:  result = 'U'; break;
		case 8:  result = 'N'; break;
		case 9:  result = 'Y'; break;
		case 10: result = 'V'; break;
		case 11: result = 'L'; break;
		case 12: result = 'P'; break;
		case 13: result = '.'; break;
	}
	return result;
}

int PrintSolution() {
	long pc, bd;
	int i, j;
	count++;
	if (count > LIMIT) return 0;
	for (pc = 1; pc <= NUM_PIECES; pc++) {
		for (bd = 0; bd <= RxC; bd++)
			if (e_inset(bd, Solution[pc - 1]->Position)) {
				A[bd % ROW + 1][bd / ROW + 1] = pc;
			}
	}
	for (i = 1; i <= ROW; i++) {
		for (j = 1; j <= COL; j++) {
			if ((A[i][j] < -1) || (A[i][j] > 13))
				printf("Oops: %d%d\n", i, j);
			else
				board[i - 1][j - 1] = conv(A[i][j]);
		}
	}
	printf("<tr>\n");
	PrintRow(format);
	printf("</tr>\n");
	return 1;
}

int BackTrack(BoardNumber k) {
	PieceNumber pc;
	if (count > LIMIT) return 0;
	while (e_inset(k, TheBoard)) k++;
	for (pc = 1; pc <= NUM_PIECES; pc++)
		if (e_inset(pc, PieceAvail)) {
			Solution[pc - 1] = List[pc - 1][k];
			PieceAvail = e_diff(PieceAvail, e_add_to_set(e_init(), pc));
			while (Solution[pc - 1] != NULL) {
				if (e_isempty(e_intersect(TheBoard, Solution[pc - 1]->Position))) {
					TheBoard = e_union(TheBoard, Solution[pc - 1]->Position);
					if (e_isempty(PieceAvail))
						PrintSolution();
					else BackTrack(k + 1);
					TheBoard = e_diff(TheBoard, Solution[pc - 1]->Position);
				} 
				Solution[pc - 1] = Solution[pc - 1]->Link;
			} 
			PieceAvail = e_add_to_set(PieceAvail, pc);
		} 
		return 1;
} 

void FindSolutions() {
	int i;
	PieceAvail = e_init();
	for(i = 1; i <= 12; i++)
		PieceAvail = e_add_to_set(PieceAvail, i);
	count = 0;
	BackTrack(Min_Square);
}

void main (int argc, char *argv[]) {
	int r, c, j;
	unsigned long pc;
	FILE *PentData;

    format = atoi(argv[1]);
	WriteOutLists = 0;
	TheBoard = e_init();
	if ((PentData = fopen(INPUT_NAME, "r")) == 0){
		printf("Can't open file: %s", INPUT_NAME);
		exit(1);
	} 
	fscanf(PentData, "%d%d%d\n", &ROW, &COL, &LIMIT);
	for (pc = 0; pc < RxC; pc++)
		TheBoard = e_add_to_set(TheBoard, pc);
	for (r = 1; r <= ROW; r++) {
		for (c = 1; c <= COL; c++) {
			fscanf(PentData, "%d", &A[r][c]);
			if (WriteOutLists) printf("%3d", A[r][c]);
			if (A[r][c] == 0)
				TheBoard = e_diff(TheBoard, e_add_to_set(e_init(),(ROW * (c - 1) + r - 1)));
		}
		fscanf(PentData,"\n");
	}
	Min_Square = 0;
	while (e_inset(Min_Square, TheBoard)) Min_Square++;
	xROW = ROW + 1;
	xCOL = COL + 1;
	if ((PentData = fopen(DATA_NAME, "r")) == 0) {
		printf("Can't open file: %s", DATA_NAME);
		exit(1);
	}
	for (pc = 0; pc < 12; pc++) {
		for( j = 0; j < 5; j++) 
			fscanf(PentData,"%d%d",&Piece[pc][j].X, &Piece[pc][j].Y);
		fscanf(PentData,"\n");
	}
	num = 0;
	GeneratePiecePositions();
	fclose(PentData);

	N = ROW;	M = COL;

    	if ((N < 0) || (N > MAXBOARD)) 
		{printf("error, invalid n = %d.\n", N); exit(-1);}
    	if ((M < 0) || (M > MAXBOARD)) 
		{printf("error, invalid m = %d.\n", M); exit(-1);}
    	if (format == 0) { /* no data specified */
		printf("<b><h3>Please specify output format</h3></b>");
		exit(-1);
    	}

   	printf("<table border=1 cellspacing=2 cellpadding=2><tr>\n");
    	if (format & 1) 
		printf("<th colspan=1><font size=+1>Letters</font><br></th>\n");
    	if (format & 2)
		printf("<th colspan=1><font size=+1>Pretty GIFs</font><br></th>\n");
    	printf("</tr>\n");

    	FindSolutions();

    	printf("</table>\n");
    	printf("Pentomino solutions = %d",count-1 );
}
