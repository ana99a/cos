<?php
  if (isset($_GET["n"]) && isset($_GET["t"]) && isset($_GET["fmt"]) && isset($_GET["num"]) && isset($_GET["gfx"])) {
    $fmt = intval($_GET["fmt"]);
    $cmd = "code/tree/tree"
                             . " " . escapeshellarg($_GET["t"])
                             . " " . min(intval($_GET["n"]), 20)  /* hard-wired maximum number of vertices */
                             . " " . strval(limit($fmt))  /* hard-wired maximum number of trees */
                             . " 2>&1"  /* this command redirects stderr to stdout */
                             . (($fmt == 2) ? " > file.txt" : "");  /* redirect output to file */                             
    exec($cmd, $result);
    $num = filter_var($_GET["num"], FILTER_VALIDATE_BOOLEAN);
    $gfx = filter_var($_GET["gfx"], FILTER_VALIDATE_BOOLEAN);
    output($result, $fmt, "string_to_list", 0, "svg_tree", (($_GET["t"] == "0") ? true : false), 0, 0, $num, $gfx);
  }
?>
